import os
from PyQt4 import QtCore, QtGui
import sqliteparser
import sys
import threading
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import bot_threading

something_running = False

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


# Builds the UI
# noinspection PyUnresolvedReferences
class Ui_Form(object):
    def __init__(self):
        self.lock = threading.Lock()
        self.db = sqliteparser.SqliteParser(self.lock)

    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(659, 516)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("images/lakbloodbot_icon.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Form.setWindowIcon(icon)
        self.horizontalLayout = QtGui.QHBoxLayout(Form)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.tabWidget = QtGui.QTabWidget(Form)
        self.tabWidget.setTabShape(QtGui.QTabWidget.Rounded)
        self.tabWidget.setDocumentMode(False)
        self.tabWidget.setTabsClosable(False)
        self.tabWidget.setMovable(False)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab_login = QtGui.QWidget()
        self.tab_login.setObjectName(_fromUtf8("tab_login"))
        self.gridLayout = QtGui.QGridLayout(self.tab_login)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.checkBox_use_for_fake = QtGui.QCheckBox(self.tab_login)
        self.checkBox_use_for_fake.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.checkBox_use_for_fake.sizePolicy().hasHeightForWidth())
        self.checkBox_use_for_fake.setSizePolicy(sizePolicy)
        self.checkBox_use_for_fake.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.checkBox_use_for_fake.setObjectName(_fromUtf8("checkBox_use_for_fake"))
        self.horizontalLayout_6.addWidget(self.checkBox_use_for_fake)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem)
        self.gridLayout.addLayout(self.horizontalLayout_6, 12, 0, 1, 1)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(self.tab_login)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.lineEdit_email = QtGui.QLineEdit(self.tab_login)
        self.lineEdit_email.setObjectName(_fromUtf8("lineEdit_email"))
        self.verticalLayout.addWidget(self.lineEdit_email)
        self.gridLayout.addLayout(self.verticalLayout, 4, 0, 1, 1)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.label_2 = QtGui.QLabel(self.tab_login)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout_2.addWidget(self.label_2)
        self.lineEdit_password = QtGui.QLineEdit(self.tab_login)
        self.lineEdit_password.setEchoMode(QtGui.QLineEdit.PasswordEchoOnEdit)
        self.lineEdit_password.setObjectName(_fromUtf8("lineEdit_password"))
        self.verticalLayout_2.addWidget(self.lineEdit_password)
        self.gridLayout.addLayout(self.verticalLayout_2, 9, 0, 1, 1)
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.label_3 = QtGui.QLabel(self.tab_login)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.verticalLayout_4.addWidget(self.label_3)
        self.lineEdit_worldid = QtGui.QLineEdit(self.tab_login)
        self.lineEdit_worldid.setObjectName(_fromUtf8("lineEdit_worldid"))
        self.verticalLayout_4.addWidget(self.lineEdit_worldid)
        self.gridLayout.addLayout(self.verticalLayout_4, 5, 0, 1, 1)
        self.box_userlist = QtGui.QComboBox(self.tab_login)
        self.box_userlist.setObjectName(_fromUtf8("box_userlist"))
        self.box_userlist.addItem(_fromUtf8(""))
        self.box_userlist.addItem(_fromUtf8(""))
        self.gridLayout.addWidget(self.box_userlist, 2, 0, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 14, 0, 1, 1)
        self.btn_delete_acc = QtGui.QPushButton(self.tab_login)
        self.btn_delete_acc.setObjectName(_fromUtf8("btn_delete_acc"))
        self.gridLayout.addWidget(self.btn_delete_acc, 2, 1, 1, 1)
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.btn_unlock_all = QtGui.QPushButton(self.tab_login)
        self.btn_unlock_all.setObjectName(_fromUtf8("btn_unlock_all"))
        self.horizontalLayout_8.addWidget(self.btn_unlock_all)
        self.label_10 = QtGui.QLabel(self.tab_login)
        self.label_10.setWordWrap(True)
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.horizontalLayout_8.addWidget(self.label_10)
        self.gridLayout.addLayout(self.horizontalLayout_8, 16, 0, 1, 1)
        self.btn_add_acc = QtGui.QPushButton(self.tab_login)
        self.btn_add_acc.setStyleSheet(_fromUtf8(""))
        self.btn_add_acc.setObjectName(_fromUtf8("btn_add_acc"))
        self.gridLayout.addWidget(self.btn_add_acc, 12, 1, 1, 1)
        self.btn_update_acc = QtGui.QPushButton(self.tab_login)
        self.btn_update_acc.setObjectName(_fromUtf8("btn_update_acc"))
        self.gridLayout.addWidget(self.btn_update_acc, 9, 1, 1, 1)
        self.btn_clear_login = QtGui.QPushButton(self.tab_login)
        self.btn_clear_login.setObjectName(_fromUtf8("btn_clear_login"))
        self.gridLayout.addWidget(self.btn_clear_login, 5, 1, 1, 1)
        self.tabWidget.addTab(self.tab_login, _fromUtf8(""))
        self.tab_fake = QtGui.QWidget()
        self.tab_fake.setObjectName(_fromUtf8("tab_fake"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.tab_fake)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.textEdit_fake_castle_link = QtGui.QTextEdit(self.tab_fake)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.textEdit_fake_castle_link.sizePolicy().hasHeightForWidth())
        self.textEdit_fake_castle_link.setSizePolicy(sizePolicy)
        self.textEdit_fake_castle_link.setStyleSheet(_fromUtf8(""))
        self.textEdit_fake_castle_link.setObjectName(_fromUtf8("textEdit_fake_castle_link"))
        self.horizontalLayout_2.addWidget(self.textEdit_fake_castle_link)
        self.btn_add_castles = QtGui.QPushButton(self.tab_fake)
        self.btn_add_castles.setObjectName(_fromUtf8("btn_add_castles"))
        self.horizontalLayout_2.addWidget(self.btn_add_castles)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.spinBox = QtGui.QSpinBox(self.tab_fake)
        self.spinBox.setObjectName(_fromUtf8("spinBox"))
        self.horizontalLayout_5.addWidget(self.spinBox)
        self.btn_delete_single_castle = QtGui.QPushButton(self.tab_fake)
        self.btn_delete_single_castle.setObjectName(_fromUtf8("btn_delete_single_castle"))
        self.horizontalLayout_5.addWidget(self.btn_delete_single_castle)
        self.btn_reload = QtGui.QPushButton(self.tab_fake)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_reload.sizePolicy().hasHeightForWidth())
        self.btn_reload.setSizePolicy(sizePolicy)
        self.btn_reload.setText(_fromUtf8("Reload"))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("images/reloadIcon.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_reload.setIcon(icon1)
        self.btn_reload.setObjectName(_fromUtf8("btn_reload"))
        self.horizontalLayout_5.addWidget(self.btn_reload)
        self.verticalLayout_3.addLayout(self.horizontalLayout_5)
        self.textEdit = QtGui.QTextEdit(self.tab_fake)
        self.textEdit.setEnabled(True)
        self.textEdit.setStyleSheet(_fromUtf8(""))
        self.textEdit.setReadOnly(True)
        self.textEdit.setObjectName(_fromUtf8("textEdit"))
        self.verticalLayout_3.addWidget(self.textEdit)
        self.btn_start_fake = QtGui.QPushButton(self.tab_fake)
        self.btn_start_fake.setObjectName(_fromUtf8("btn_start_fake"))
        self.verticalLayout_3.addWidget(self.btn_start_fake)
        self.tabWidget.addTab(self.tab_fake, _fromUtf8(""))
        self.tab_castle_check = QtGui.QWidget()
        self.tab_castle_check.setObjectName(_fromUtf8("tab_castle_check"))
        self.verticalLayout_8 = QtGui.QVBoxLayout(self.tab_castle_check)
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))
        self.verticalLayout_7 = QtGui.QVBoxLayout()
        self.verticalLayout_7.setObjectName(_fromUtf8("verticalLayout_7"))
        self.label_9 = QtGui.QLabel(self.tab_castle_check)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_9.sizePolicy().hasHeightForWidth())
        self.label_9.setSizePolicy(sizePolicy)
        self.label_9.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_9.setTextFormat(QtCore.Qt.RichText)
        self.label_9.setScaledContents(False)
        self.label_9.setWordWrap(True)
        self.label_9.setIndent(1)
        self.label_9.setOpenExternalLinks(False)
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.verticalLayout_7.addWidget(self.label_9)
        self.btn_start_castle_expansion = QtGui.QPushButton(self.tab_castle_check)
        self.btn_start_castle_expansion.setObjectName(_fromUtf8("btn_start_castle_expansion"))
        self.verticalLayout_7.addWidget(self.btn_start_castle_expansion)
        self.verticalLayout_8.addLayout(self.verticalLayout_7)
        self.tabWidget.addTab(self.tab_castle_check, _fromUtf8(""))
        self.tab_settings = QtGui.QWidget()
        self.tab_settings.setObjectName(_fromUtf8("tab_settings"))
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.tab_settings)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_4 = QtGui.QLabel(self.tab_settings)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_4.addWidget(self.label_4)
        self.lineEdit_attack_min = QtGui.QLineEdit(self.tab_settings)
        self.lineEdit_attack_min.setObjectName(_fromUtf8("lineEdit_attack_min"))
        self.horizontalLayout_4.addWidget(self.lineEdit_attack_min)
        self.label_5 = QtGui.QLabel(self.tab_settings)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.horizontalLayout_4.addWidget(self.label_5)
        self.lineEdit_attack_max = QtGui.QLineEdit(self.tab_settings)
        self.lineEdit_attack_max.setObjectName(_fromUtf8("lineEdit_attack_max"))
        self.horizontalLayout_4.addWidget(self.lineEdit_attack_max)
        self.label_6 = QtGui.QLabel(self.tab_settings)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.horizontalLayout_4.addWidget(self.label_6)
        self.verticalLayout_5.addLayout(self.horizontalLayout_4)
        self.checkBox_maxSpeed = QtGui.QCheckBox(self.tab_settings)
        self.checkBox_maxSpeed.setTristate(False)
        self.checkBox_maxSpeed.setObjectName(_fromUtf8("checkBox_maxSpeed"))
        self.verticalLayout_5.addWidget(self.checkBox_maxSpeed)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_7 = QtGui.QLabel(self.tab_settings)
        self.label_7.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.horizontalLayout_3.addWidget(self.label_7)
        self.dateTimeEdit_attack = QtGui.QDateTimeEdit(self.tab_settings)
        self.dateTimeEdit_attack.setDateTime(QtCore.QDateTime(QtCore.QDate(2016, 1, 1), QtCore.QTime(8, 0, 0)))
        self.dateTimeEdit_attack.setObjectName(_fromUtf8("dateTimeEdit_attack"))
        self.horizontalLayout_3.addWidget(self.dateTimeEdit_attack)
        self.verticalLayout_5.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        self.label_8 = QtGui.QLabel(self.tab_settings)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.horizontalLayout_7.addWidget(self.label_8)
        self.lineEdit_fake_amount = QtGui.QLineEdit(self.tab_settings)
        self.lineEdit_fake_amount.setObjectName(_fromUtf8("lineEdit_fake_amount"))
        self.horizontalLayout_7.addWidget(self.lineEdit_fake_amount)
        self.verticalLayout_5.addLayout(self.horizontalLayout_7)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_5.addItem(spacerItem2)
        self.verticalLayout_6.addLayout(self.verticalLayout_5)
        self.tabWidget.addTab(self.tab_settings, _fromUtf8(""))
        self.horizontalLayout.addWidget(self.tabWidget)

        self.retranslateUi(Form)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Form)
        Form.setTabOrder(self.lineEdit_worldid, self.box_userlist)
        Form.setTabOrder(self.box_userlist, self.tabWidget)
        Form.setTabOrder(self.tabWidget, self.lineEdit_email)
        Form.setTabOrder(self.lineEdit_email, self.lineEdit_password)
        Form.setTabOrder(self.lineEdit_password, self.btn_delete_acc)
        Form.setTabOrder(self.btn_delete_acc, self.btn_add_castles)
        Form.setTabOrder(self.btn_add_castles, self.btn_start_fake)
        Form.setTabOrder(self.btn_start_fake, self.lineEdit_attack_min)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Lak Blood Bot", None))
        self.checkBox_use_for_fake.setText(_translate("Form", "Use this account for fakes?", None))
        self.label.setText(_translate("Form", "E - mail", None))
        self.label_2.setText(_translate("Form", "password", None))
        self.label_3.setText(_translate("Form", "WorldID", None))
        self.box_userlist.setItemText(0, _translate("Form", "User1", None))
        self.box_userlist.setItemText(1, _translate("Form", "asdda", None))
        self.btn_delete_acc.setText(_translate("Form", "Delete", None))
        self.btn_unlock_all.setToolTip(_translate("Form",
                                                  "This will unlock them all, however if you press it while they are running it will crash all currently running actions.",
                                                  None))
        self.btn_unlock_all.setText(_translate("Form", "Unlock all (DEBUG ONLY)", None))
        self.label_10.setToolTip(_translate("Form",
                                            "This will unlock them all, however if you press it while they are running it will crash all currently running actions.",
                                            None))
        self.label_10.setText(_translate("Form",
                                         "<html><head/><body><p>Press this button if your program has crashed somehow and the accounts are still locked.</p></body></html>",
                                         None))
        self.btn_add_acc.setText(_translate("Form", "Add new", None))
        self.btn_update_acc.setText(_translate("Form", "Update", None))
        self.btn_clear_login.setText(_translate("Form", "Clear", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_login), _translate("Form", "Login", None))
        self.btn_add_castles.setText(_translate("Form", "Add Castles", None))
        self.btn_delete_single_castle.setText(_translate("Form", "Delete All Entries", None))
        self.btn_start_fake.setText(
            _translate("Form", "Start to fake all castles - Visit the next tab beforehand.", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_fake), _translate("Form", "Fake", None))
        self.label_9.setText(_translate("Form",
                                        "<html><head/><body><p>Before you start to fake, you have to start the Expanding of your castle, so it can save the speed modifiers of all of your castles. This should be done before every major fakeing session because if you don\'t do it, the dates might be really far off. It mainly has to be redone after a finished research, that affected the movement speed of your castle.</p><p>To start this event just press the button down below.</p><p>This can take quiet some time, so just be patient, my Patient. ^^</p></body></html>",
                                        None))
        self.btn_start_castle_expansion.setText(_translate("Form", "PushButton", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_castle_check),
                                  _translate("Form", "Castle Check", None))
        self.label_4.setText(_translate("Form", "Time to sleep between attacks? From ", None))
        self.lineEdit_attack_min.setText(_translate("Form", "7", None))
        self.label_5.setText(_translate("Form", "to", None))
        self.lineEdit_attack_max.setText(_translate("Form", "10", None))
        self.label_6.setText(_translate("Form", "Seconds", None))
        self.checkBox_maxSpeed.setText(
            _translate("Form", "Do you want to want the maximum speed? This eliminates the use of the first Textbox.",
                       None))
        self.label_7.setText(_translate("Form", "When do you want you attacks to reach the target?", None))
        self.dateTimeEdit_attack.setDisplayFormat(_translate("Form", "dd.MM.yyyy HH:mm", None))
        self.label_8.setText(_translate("Form", "From how many castles do you want to start your fakes?", None))
        self.lineEdit_fake_amount.setText(_translate("Form", "30", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_settings), _translate("Form", "Settings", None))

        self.own_setup()
        self.link_buttons()

        # print(self.textEdit.insertHtml('<span style=" color:#ff0000;">Red</span>'))

    # links.py the buttons to the methodes
    def link_buttons(self):
        self.btn_add_castles.clicked.connect(self.add_fake_castles)
        self.btn_delete_single_castle.clicked.connect(self.delete_single_castle)
        self.box_userlist.currentIndexChanged.connect(self.dropdown_changed)
        self.btn_start_fake.clicked.connect(self.start_fake)
        self.btn_start_castle_expansion.clicked.connect(self.start_expansion)
        self.btn_unlock_all.clicked.connect(self.unlock_all)
        self.btn_reload.clicked.connect(self.populate_fake_text)

        # Account management buttons
        self.btn_delete_acc.clicked.connect(self.delete_account)
        self.btn_add_acc.clicked.connect(self.add_account)
        self.btn_update_acc.clicked.connect(self.update_account)
        self.btn_clear_login.clicked.connect(self.clear_login)

        # Updates the settings if you change them
        self.lineEdit_attack_max.editingFinished.connect(
            lambda: self.db.update_setting("attack_sleep_max", self.lineEdit_attack_max.text()))

        self.lineEdit_attack_min.editingFinished.connect(
            lambda: self.db.update_setting("attack_sleep_min", self.lineEdit_attack_min.text()))

        self.lineEdit_fake_amount.editingFinished.connect(
            lambda: self.db.update_setting("fake_castle_amount", self.lineEdit_fake_amount.text()))

        self.dateTimeEdit_attack.editingFinished.connect(
            lambda: self.db.update_setting("attack_date", self.dateTimeEdit_attack.text()))

        self.checkBox_maxSpeed.stateChanged.connect(
            lambda: self.db.update_setting("max_speed", int(self.checkBox_maxSpeed.isChecked())))

    def own_setup(self):
        self.textEdit.setFontPointSize(10)
        self.populate_dropdown_from_db()
        self.populate_fake_text()
        self.dropdown_changed()
        self.populate_settings()

    @staticmethod
    def test():
        print("Got fired")

    # Fake Management
    def add_fake_castles(self):
        links = self.textEdit_fake_castle_link.toPlainText().splitlines()
        for link in links:
            link = link.replace(" ", "")
            if link.startswith("l+k://coordinates?"):
                self.db.add_castle_fake(link)
                self.populate_fake_text()

    # deletes single fake with index
    def delete_single_castle(self):
        self.db.delete_all_fakes()
        self.populate_fake_text()

    # sets the text with the fakes
    def populate_fake_text(self):
        all_castles = self.db.get_all_fakes()
        fake_text_html = ""
        for a in all_castles:
            fake_text_html += str(a[0]) + ": "
            fake_text_html += '<span style=" color:#0000ff;">' + str(a[2]) + '</span>'
            if a[4] == 0:
                fake_text_html += " will be faked"
            elif a[4] == 1:
                fake_text_html += " is currently being faked"
            elif a[4] == 2:
                fake_text_html += " has been faked from "
                fake_text_html += '<span style=" color:#00ff00;">' + str(a[3]) + '</span>'
                fake_text_html += " Castles by "
                fake_text_html += '<span style=" color:#ff5500;">' + str(a[5]) + '</span>'
            fake_text_html += "<br>"
        self.textEdit.setHtml(fake_text_html)

    # sets the settings textfields to the real settings
    def populate_settings(self):
        self.lineEdit_attack_min.setText(self.db.get_setting("attack_sleep_min"))
        self.lineEdit_attack_max.setText(self.db.get_setting("attack_sleep_max"))
        self.lineEdit_fake_amount.setText(self.db.get_setting("fake_castle_amount"))
        self.dateTimeEdit_attack.setDateTime(
            QtCore.QDateTime.fromString(self.db.get_setting("attack_date"), "dd.MM.yyyy hh:mm"))
        self.checkBox_maxSpeed.setChecked(bool(int(self.db.get_setting("max_speed"))))

        # date_str = self.dateTimeEdit_attack.dateTime().toString(
        #   self.dateTimeEdit_attack.dateTime().toString("dd.MM.yyyy hh:mm"))
        # d = QtCore.QDateTime.fromString(date_str, "dd.MM.yyyy hh:mm")
        # print(d)

    def save_settings(self, setting, value):
        self.db.set_setting(setting, value)

    # Account managing
    def update_account(self):
        current_acc = self.get_selected_account()
        email = self.lineEdit_email.text()
        password = self.lineEdit_password.text()
        worldid = self.lineEdit_worldid.text()
        self.db.remove_user_from_db(current_acc[0], current_acc[1])
        self.db.add_user(email, email, password, worldid, int(self.checkBox_use_for_fake.isChecked()))
        self.populate_dropdown_from_db()
        self.box_userlist.setCurrentIndex(self.box_userlist.count() - 1)

    def clear_login(self):
        self.lineEdit_email.setText("")
        self.lineEdit_password.setText("")
        self.lineEdit_worldid.setText("")
        self.checkBox_use_for_fake.setChecked(True)

    def add_account(self):
        email = self.lineEdit_email.text()
        password = self.lineEdit_password.text()
        worldid = self.lineEdit_worldid.text()
        self.db.add_user(email, email, password, worldid, int(self.checkBox_use_for_fake.isChecked()))
        self.populate_dropdown_from_db()
        self.box_userlist.setCurrentIndex(self.box_userlist.count() - 1)

    def delete_account(self):
        c = self.get_selected_account()
        self.db.remove_user_from_db(c[0], c[1])
        self.populate_dropdown_from_db()

    # gets the currently selected account in the dropdown menu
    def get_selected_account(self):
        current = self.box_userlist.currentText().split(" , ")
        return current

    # sets the dropdown to the data from the db
    def populate_dropdown_from_db(self):
        self.box_userlist.clear()
        users = self.db.read_userdata_from_db()
        for i in users:
            self.box_userlist.addItem(i[1] + " , " + i[3])

    # populates the text fields if you select something from the dropdown
    def dropdown_changed(self):
        try:
            account = self.db.read_specific_user(self.get_selected_account()[0], self.get_selected_account()[1])
            self.lineEdit_email.setText(account[1])
            self.lineEdit_password.setText(account[2])
            self.lineEdit_worldid.setText(account[3])
            self.checkBox_use_for_fake.setChecked(bool(account[4]))
            self.populate_fake_text()
        except IndexError:
            pass

    # starts the fake when pressing the button
    def start_fake(self):
        bot_threading.start_bot("fake")

    # starts the expansion of the castles if you press the other button
    def start_expansion(self):
        bot_threading.start_bot("expand")

    # unlocks all the accounts
    def unlock_all(self):
        self.db.unlock_all()


# Checks if the code of the program is real, or if it has been disabled
def check_code():
    # As Debug, you can setup a file that has the firefox path in it
    line = None
    if os.path.isfile("firefox_dir.txt"):
        with open("firefox_dir.txt") as f:
            line = f.readline().replace("\\", "/")
            print(str(line))
    browser = webdriver.Firefox(executable_path=line)
    browser.get("https://sites.google.com/site/bloodworkxbotkey/1_1")
    try:
        browser.implicitly_wait(1)
        browser.find_element_by_xpath("//*[contains(text(), '3j4981j2983j98jajs98dg98uwr435j9')]")
        print("Code valid")
        browser.close()
    except NoSuchElementException:
        print("Wrong Key, please ask the developer for a newer version.")
        browser.quit()
        quit()


# Only runs if the window is main
if __name__ == "__main__":
    check_code()
    # builds and starts the Gui
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    app.exec_()
    # Closes off the programm
    sys.exit()
