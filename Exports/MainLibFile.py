from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import time
import datetime

# User Data

version_number = "1_1"
yourLoginEmail = 'oemer.okur69@outlook.com'
yourLoginPassword = 'fenerli69'
yourWorldID = "118"
dateWhenFinished = datetime.datetime(2016, 12, 30, 20, 0)
runXTimes = -1
if True:
    yourLoginEmail = 'bloodworkxgaming@gmail.com'
    yourLoginPassword = 'shadow123'
# 21-12-2015 20:20


# static Data
missionDic = {27: 10, 28: 78, 29: 300, 11: 1440, 12: 144, 7: 720, 13: 600, 14: 120, 15: 180, 16: 300, 17: 420,
              18: 480, 19: 540, 20: 660, 21: 600, 22: 780, 23: 900, 24: 1080, 25: 1440, 26: 960}
buildingMaxLevelDic = {1: 10, 1100: 10, 1000: 10, 1300: 30, 800: 30, 100: 30, 300: 30, 500: 30,
                       900: 8, 1200: 20, 200: 20, 400: 20, 600: 20}
unit_speed = {1: 11.1, 2: 12.7, 101: 7.9, 102: 9.5, 201: 4.8, 202: 6.3}

# implements webdriver
browser = webdriver.Firefox()
# browser = webdriver.PhantomJS()
browser.set_window_size(1400, 800)
browser.get("https://sites.google.com/site/bloodworkxbotkey/" + version_number)
try:
    browser.find_elements_by_xpath("//*[contains(text(), '3j4981j2983j98jajs98dg98uwr435j9')]")
except Exception:
    print("Wrong Key, please ask the developer for a newer version.")
    quit()
browser.get('http://lordsandknights.com/')


# # Asks for the User data
# print("welcome to the LAK bot.")
# yourLoginEmail = input("Type your Email Address:\n")
# yourLoginPassword = input("Type your Password:\n")
# yourWorldID = input("Type your world ID: (118 for D15)\n")
# runXTimes = int(input("How many times to do you want to run the loop? Every 12 min 1 loop. -1 for infinite\n"))
# dateWhenFinished = datetime.datetime.strptime(
#         input("When do you want the last mission to be finished\nFormat: DD-MM-YYYY MM:HH\n"), "%d-%m-%Y %H:%M")


# General Functions
def wait_for_element(element_name, index=0, by='class name', timeout_time=80):
    # waits until the missions button is clickable
    ready = False
    times_run = 0
    while not ready:
        time.sleep(.2)
        try:
            waiting_for = browser.find_elements(by, element_name)[index]
            print("\nPage is ready!")
            ready = True
            return waiting_for
        except NoSuchElementException:
            if times_run == 0:
                print("( " + element_name + ") Element not found!", end="", flush=True)
            else:
                if times_run > 0:
                    print("!", end="", flush=True)
            times_run += 1
        except IndexError:
            if times_run == 0:
                print("( " + element_name + ") Element not found!", end="", flush=True)
            else:
                if times_run > 0:
                    print("!", end="", flush=True)
        times_run += 1
        if times_run > timeout_time:
            raise LookupError("Page not loading. waiting for element: '" + str(element_name) + "'")


def login(login_email, login_password, world_id):
    wait_for_element("popup-link").click()
    time.sleep(.4)

    # inserts email address
    elem = wait_for_element("loginEmail", 0, "name")
    elem.clear()
    elem.send_keys(login_email + Keys.TAB)
    time.sleep(.5)
    # inserts password and presses enter
    wait_for_element("loginPassword", 0, "name").send_keys(login_password + Keys.RETURN)

    # waits until the worldbutton is clickable
    wait_for_element(world_id, 0, "id").click()
    print("Logged in, load page")


def overlay_appeared():
    # waits until the overlay disappears
    times_run = 0
    while True:
        time.sleep(.2)
        try:
            browser.find_element_by_class_name("overlay")
            if times_run == 0:
                print("Overlay still there!", end="", flush=True)
            else:
                if times_run > 0:
                    print("!", end="", flush=True)
            times_run += 1
        except NoSuchElementException:
            print("\nOverlay disappeared!")
            return
    time.sleep(1)


def close_all_windows():
    try:
        # while len(all_x) > 0:
        all_x = browser.find_elements_by_class_name("close")
        for x in all_x:
            print("Closing all Windows")
            if x.is_displayed():
                x.click()
    # all_x = browser.find_elements_by_class_name("close")
    except:
        print("all windows already Closed")

    time.sleep(.2)
    # finds all Xes and clicks them
    # all_x = [1]


def open_castle(link):
    close_all_windows()
    browser.execute_script("window.linkHandler('" + str(link) + "')")
    overlay_appeared()
    time.sleep(2)
    canvas = browser.find_element_by_id("map")
    castle_click_action = ActionChains(browser).move_by_offset(0, 0).click(canvas)
    castle_click_action.perform()
    overlay_appeared()
    time.sleep(1)


def accept_popup():
    time.sleep(.3)
    try:
        browser.find_elements_by_xpath('//div[contains(@class, "win dialog")]//*[@class="button"]').click()
        time.sleep(.5)
        return True
    except:
        return False


def reset_browser():
    browser.get('http://lordsandknights.com')
    time.sleep(.1)
    try:
        alert = browser.switch_to_alert()
        alert.accept()
    except:
        print("no popup window appeared")
    overlay_appeared()

if __name__ == "__main__":
    login(yourLoginEmail, yourLoginPassword, yourWorldID)
