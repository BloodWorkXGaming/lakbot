import random
import matplotlib.pyplot as plt

start_value = 10
last_payed = start_value
total_money = 1000
lst = []
plot_length = 200000
# Chance to win. 1 To Value
chance = .486
# What is the payout multiplier?
win_multiplier = 2

for i in range(plot_length):
    total_money -= last_payed
    if random.random() < chance:
        total_money += last_payed * win_multiplier
        last_payed = start_value
    else:
        last_payed *= 2

    # if last_payed > 2000:
    #     last_payed /= 200


    lst.append(total_money)
    # lst.append(random.random() * 1000)
    # print(total_money)

plt.plot([0] * plot_length)
plt.plot([1000] * plot_length)
l = plt.plot(lst)
# plt.setp(l, color="r", linewidth=1.0, )

plt.show()
