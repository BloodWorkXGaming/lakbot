import sqlite3
import datetime
import time
import random
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib import style
style.use("fivethirtyeight")

conn = sqlite3.connect("default.db")
c = conn.cursor()


def create_table():
    c.execute("CREATE TABLE IF NOT EXISTS stuffToPlot(unix REAL, datestamp TEXT, keyword TEXT, value REAL)")


def dynamic_data_entry():
    for i in range(20):
        unix = time.time()
        date = str(datetime.datetime.fromtimestamp(unix).strftime("%Y-%m-%d %H:%M:%S"))
        keyword = "python"
        value = random.randrange(0, 10)
        c.execute("INSERT INTO stuffToPlot (unix, datestamp, keyword, value) VALUES (?, ?, ?, ?)",
                  (unix, date, keyword, value))
        time.sleep(.5)
    conn.commit()


def read_from_db():
    c.execute("SELECT * FROM stuffToPlot WHERE value >= 8")
    for row in c.fetchall():
        print(row)


def graph_data():
    c.execute("SELECT unix, value FROM stuffToPlot")
    dates = []
    values = []
    for row in c.fetchall():
        # print(row[0])
        # print(datetime.datetime.fromtimestamp(row[0]))
        dates.append(datetime.datetime.fromtimestamp(row[0]))
        values.append(row[1])
    plt.plot_date(dates, values, "-")
    plt.show()

# dynamic_data_entry()

graph_data()

c.close()
conn.close()