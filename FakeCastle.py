from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import time
import random

# static data
yourLoginEmail = 'bloodworkxgaming@gmail.com'
yourLoginPassword = ''
yourWorldID = "118"


# inplements webdriver
browser = webdriver.Firefox()
browser.get('http://lordsandknights.com/de/game-login-2/')

# Asks for the User data
yourLoginEmail = input("Type your Email Address:\n")
yourLoginPassword = input("Type your Password:\n")
yourWorldID = input("Type your world ID: (118 for D15)\n")


def login(login_email, login_password, world_id):
    # inserts email address
    time.sleep(.5)
    elem = browser.find_element_by_name("loginEmail")
    elem.clear()
    elem.send_keys(login_email)

    # inserts password and presses enter
    elem = browser.find_element_by_name("loginPassword")
    elem.send_keys(login_password + Keys.RETURN)

    # waits until the worldbutton is clickable
    ready = False
    while not ready:
        time.sleep(.1)
        try:
            browser.find_element_by_id(world_id)
            print("Page is ready!")
            ready = True
        except NoSuchElementException:
            print("Element not found!")
    browser.find_element_by_id(world_id).click()


def waitForElement(elementName, index):
    # waits until the missions button is clickable
    ready = False
    while not ready:
        time.sleep(.2)
        try:
            waitingFor = browser.find_elements_by_class_name(elementName)[index]
            print("Page is ready!")
            ready = True
            return waitingFor
        except NoSuchElementException:
            print("Element not found!")
        except IndexError:
            print("Element not found!")


def closeAllWindows():
    # finds all Xes and clicks them
    try:
        for x in browser.find_elements_by_class_name("close"):
            if x.is_displayed():
                x.click()
    except:
        print("all windows already Closed")


def fake_from_all_castles():
    waitForElement("transports", 0).click()
    waitForElement("sync", 0).click()
    time.sleep(2)
    own_castles_text_field = browser.find_elements_by_xpath('//*[@class="sendStuffList"]/*[@class="listItem"]')
    castleCount = 0
    totalAmount = len(own_castles_text_field)
    for ownCastle in own_castles_text_field:

        own_castle_swords = ownCastle.find_element_by_xpath("./div[2]/div[2]/div[3]/div[1]/input[1]")
        castleCount += 1
        print("Castle: " + str(castleCount) + " of " + str(totalAmount))
        if random.randint(1, 5) > 2:
            print("Skipped this castle\n")
            continue
        if own_castle_swords.get_attribute("disabled") != "true":
            own_castle_swords.clear()
            own_castle_swords.send_keys('1')
            time.sleep(.1)
            while True:
                time.sleep(.05)
                ownCastle.find_element_by_xpath('.//*[@class="button action syncAttack"]').click()
                try:
                    browser.find_element_by_xpath(
                            '//div[contains(@class, "win foreignAction")]//div[@class="overlay"]')
                    break
                except NoSuchElementException:
                    pass
            print("Faked From Castle: " + ownCastle.find_element_by_class_name("title").text)
            sl = 8 + random.randint(0, 4)
            print("Waiting " + str(sl) + " seconds for the next attack\n")
            time.sleep(sl)
        else:
            own_castle_bows = ownCastle.find_element_by_xpath("./div[2]/div[3]/div[3]/div[1]/input[1]")
            if own_castle_bows.get_attribute("disabled") != "true":
                own_castle_bows.clear()
                own_castle_bows.send_keys('1')
                time.sleep(.4)
                while True:
                    time.sleep(.05)
                    ownCastle.find_element_by_xpath('.//*[@class="button action syncAttack"]').click()
                    try:
                        browser.find_element_by_xpath(
                                '//div[contains(@class, "win foreignAction")]//div[@class="overlay"]')
                        break
                    except NoSuchElementException:
                        pass
                print("Faked From Castle: " + ownCastle.find_element_by_class_name("title").text)
                sl = 8 + random.randint(0, 4)
                print("Waiting " + str(sl) + " seconds for the next attack\n")
                time.sleep(sl)
            else:
                print("Castle not in range\n")
    #closeAllWindows()


login(yourLoginEmail, yourLoginPassword, yourWorldID)


while True:
    print("Click on the Castle you want to fake")
    print("You have to have one attack running")
    inp = input('press "Enter" if you have clicked the castle once\n')
    if inp == "stop":
        break
    fake_from_all_castles()
