import sys
from PyQt4 import QtGui, QtCore



class Window(QtGui.QMainWindow):

    def __init__(self):
        super(Window, self).__init__()
        self.setGeometry(50, 50, 500, 300)
        self.setWindowTitle("WUHU")
        self.setWindowIcon(QtGui.QIcon("icons/favicon.ico"))
        self.home()

    def home(self):
        btn = QtGui.QPushButton("Quit", self)
        btn.clicked.connect(self.close_application)
        btn.resize(100, 50)


        self.show()

    def close_application(self):
        print("got exited")
        sys.exit()

    def p(self):
        print("whoho")


def run():
    app = QtGui.QApplication(sys.argv)
    GUI = Window()
    sys.exit(app.exec_())


run()
