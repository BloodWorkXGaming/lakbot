from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import time
import math
import MainLibFile as Lak


# static data
yourLoginEmail = 'bloodworkxgaming@gmail.com'
yourLoginPassword = ''
yourWorldID = "118"

# implements webdriver
browser = Lak.browser

# Asks for the User data
yourLoginEmail = input("Type your Email Address:\n")
yourLoginPassword = input("Type your Password:\n")
yourWorldID = input("Type your world ID: (118 for D15)\n")


def wait_for_element(element_name, index=0, by='class name'):
    # waits until the missions button is clickable
    ready = False
    times_run = 0
    while not ready:
        time.sleep(.2)
        try:
            waiting_for = browser.find_elements(by, element_name)[index]
            print("\nPage is ready!")
            ready = True
            return waiting_for
        except NoSuchElementException:
            if times_run == 0:
                print("Element not found!", end="", flush=True)
            else:
                if times_run > 0:
                    print("!", end="", flush=True)
            times_run += 1
        except IndexError:
            if times_run == 0:
                print("Element not found!", end="", flush=True)
            else:
                if times_run > 0:
                    print("!", end="", flush=True)
        times_run += 1
        if times_run > 80:
            raise LookupError("Page not loading. waiting for element: " + str(element_name))


def close_all_windows():
    # finds all Xes and clicks them
    try:
        for x in browser.find_elements_by_class_name("close"):
            if x.is_displayed():
                x.click()
    except:
        print("all windows already Closed")


def collect_silver():
    times_run = 0
    while times_run < 2:
        times_run += 1
        own_castles_textfield = browser.find_elements_by_xpath('//*[@class="sendStuffList"]/*[@class="listItem"]')
        castle_count = 0
        total_amount = len(own_castles_textfield)
        # goes through every castle
        loopNum = 0
        for ownCastle in own_castles_textfield:
            # try:
            own_castle_silver = ownCastle.find_element_by_xpath("./div[4]/div[5]/div[3]/div[2]")
            own_castle_silver.click()
            own_castle_silver.click()
            max_silver = int(own_castle_silver.get_attribute("data-max"))
            castle_count += 1
            print("Castle: " + str(castle_count) + " of " + str(total_amount))
            own_castle_horseman = ownCastle.find_element_by_xpath("./div[2]/div[5]/div[3]/div[1]/input")
            # calculates the amount of armored horsemen are needed
            needed_horsemen = max_silver / 22
            if needed_horsemen > float(own_castle_horseman.get_attribute("placeholder")):
                needed_horsemen = own_castle_horseman.get_attribute("placeholder")
            needed_horsemen = int(math.ceil(float(needed_horsemen)))
            if needed_horsemen > 0:
                own_castle_horseman.clear()
                own_castle_horseman.send_keys(needed_horsemen)
                time.sleep(.3)
                while True:
                    time.sleep(.05)
                    ownCastle.find_element_by_class_name('sendResources').click()
                    if Lak.accept_popup():
                        ownCastle.find_element_by_xpath("./div[4]/div[5]/div[3]/div[2]").click()
                    try:
                        browser.find_element_by_xpath(
                                '//div[contains(@class, "win foreignAction sendResources frame-container")]'
                                '//div[@class="overlay"]')
                        break
                    except NoSuchElementException:
                        time.sleep(.1)
                        pass
                Lak.overlay_appeared()
                time.sleep(.5)
            # except:s
            #    print("could not send silver. moving on.")
            loopNum += 1


Lak.login(yourLoginEmail, yourLoginPassword, yourWorldID)

while True:
    print("Click on the Castle you want to collect the Silver at")
    print("You have to have one attack running")
    inp = input('press "Enter" if you have entered the "send resources" Menu\n')
    if inp == "exit":
        break
    collect_silver()
