from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
import time
import datetime
import MainLibFile as Lak


browser = Lak.browser

# User Data
version_number = "1_1"
yourLoginEmail = 'oemer.okur69@outlook.com'
yourLoginPassword = 'fenerli69'
yourWorldID = "118"
dateWhenFinished = datetime.datetime(2016, 12, 30, 20, 0)
runXTimes = -1
if True:
    yourLoginEmail = 'bloodworkxgaming@gmail.com'
    yourLoginPassword = 'shadow123'
# 21-12-2015 20:20

# static Data
missionDic = {27: 10, 28: 78, 29: 300, 11: 1440, 12: 144, 7: 720, 13: 600, 14: 120, 15: 180, 16: 300, 17: 420,
              18: 480, 19: 540, 20: 660, 21: 600, 22: 780, 23: 900, 24: 1080, 25: 1440, 26: 960}
buildingMaxLevelDic = {1: 10, 1100: 10, 1000: 10, 1300: 30, 800: 30, 100: 30, 300: 30, 500: 30,
                       900: 8, 1200: 20, 200: 20, 400: 20, 600: 20}

# # Asks for the User data
# print("welcome to the LAK bot.")
# yourLoginEmail = input("Type your Email Address:\n")
# yourLoginPassword = input("Type your Password:\n")
# yourWorldID = input("Type your world ID: (118 for D15)\n")
# runXTimes = int(input("How many times to do you want to run the loop? Every 12 min 1 loop. -1 for infinite\n"))
# dateWhenFinished = datetime.datetime.strptime(
#         input("When do you want the last mission to be finished\nFormat: DD-MM-YYYY MM:HH\n"), "%d-%m-%Y %H:%M")


# General Functions

def rerun_if_session_ended():
    try:
        sessionEnded = browser.find_element_by_class_name("dialog")
        reset_browser()
        run()
    except:
        print("running good.")


def close_all_windows():
    # finds all Xes and clicks them
    try:
        for x in browser.find_elements_by_class_name("close"):
            if x.is_displayed():
                x.click()
    except:
        print("all windows already Closed")


# Mission Functions, not used anymore
def start_all_missions():
    # waits for the top toolbar to appear
    Lak.wait_for_element("topbarImageContainer", 6).click()

    # waits until the missions window has opened
    Lak.wait_for_element("selectAllButton", 0)

    # selects all missions and starts them
    browser.find_element_by_class_name("selectAllButton").click()

    # time.sleep(.3)

    browser.find_element_by_class_name("execute").click()

    Lak.overlay_appeared()
    browser.find_elements_by_class_name("tab-castle-fortess")[1].click()

    time.sleep(.2)
    browser.find_element_by_class_name("selectAllButton").click()
    browser.find_element_by_class_name("execute").click()


def calculate_time_left():
    now = datetime.datetime.now()
    print("Now: " + str(now))
    print("Date: " + str(dateWhenFinished))
    timeLeft = dateWhenFinished - now
    print("time left: " + str(timeLeft))
    timeLeftInMin = timeLeft.total_seconds() / 60
    print(timeLeft.total_seconds())
    # timeLeftInMin = timeLeftInMin.seconds / 60
    print("time left till date: " + str(timeLeftInMin))
    return int(timeLeftInMin)


timeLeft = calculate_time_left()


def start_all_missions_in_time():
    close_all_windows()
    # waits for the top toolbar to appear
    Lak.wait_for_element("topbarImageContainer", 6).click()
    time.sleep(.5)
    # changes to castle tab
    has_fort = True
    try:
        # this spelling mistake is in the HTML Code:
        browser.find_elements_by_class_name("tab-castle-fortess")[0].click()
    except NoSuchElementException:
        print("no fortress yet")
        has_fort = False
    except IndexError:  #
        print("no fortress yet")
        has_fort = False
    # goes through all Missions
    all_missions = browser.find_elements_by_class_name("checkboxWrapper")
    for currentMission in all_missions:
        if timeLeft > missionDic[int(currentMission.get_attribute("data-mission"))]:
            if currentMission.is_selected() == False and currentMission.is_displayed() == True:
                currentMission.click()
    # starts them
    browser.find_element_by_class_name("execute").click()
    Lak.overlay_appeared()

    # changes to fortress tab
    if has_fort:
        # This spelling Mistake is in the code
        browser.find_elements_by_class_name("tab-castle-fortess")[1].click()
        # goes through all Missions, again
        all_missions = browser.find_elements_by_class_name("checkboxWrapper")
        for currentMission in all_missions:
            if timeLeft > missionDic[int(currentMission.get_attribute("data-mission"))]:
                if currentMission.is_selected() == False and currentMission.is_displayed() == True:
                    currentMission.click()
        browser.find_element_by_class_name("execute").click()
        Lak.overlay_appeared()


# Castle Building Help Function, get stopped each time when a building was expanded, and started again,
# stopped after a full run without any expansions
def build_castles_help():
    all_castles = browser.find_elements_by_xpath('//*[@id="globalBuildingList"]//*[@class="listContentRow line"]')
    amount_castles_expanded = 0
    # goes through all castles
    for singleCastle in all_castles:
        # looks at every castle and watches whether it can be expanded
        current_expansions = 0
        try:
            current_expansions = len(singleCastle.find_elements_by_class_name("upgrade"))
        except:
            print("already 2 expansions running")
        try:
            currently_expandable = len(singleCastle.find_elements_by_class_name("buildbutton"))
        except:
            print("no Buildings can be expanded")
            currently_expandable = 0

        # if it can be expanded:
        if current_expansions < 2 and currently_expandable > 0:
            print("\n\ncastle can be used")
            all_buildings = singleCastle.find_elements_by_class_name("building")
            smallest_building = [1, None]

            # goes through every building and looks what the smalles building is
            for building in all_buildings:
                building_level = int(building.find_element_by_class_name("level").text)
                print("\nbuilding Level: " + str(building_level))
                # buildingID = None
                build_button = None
                # percentToMax = None
                # calculates the percent to the Max build Level
                try:
                    build_button = building.find_element_by_class_name("buildbutton")
                    building_id = int(build_button.get_attribute("data-current-building")) - building_level
                    percent_to_max = \
                        (int(build_button.get_attribute("data-building")) - building_id) / int(
                                buildingMaxLevelDic[building_id])
                    print("Building max lvl: " + str(buildingMaxLevelDic[building_id]))
                except NoSuchElementException:
                    print("this building can not be expanded")
                    percent_to_max = 1.0
                print("percent To Max: " + str(percent_to_max))
                # looks whether the currently checked building is lower than the already lowest
                if percent_to_max < smallest_building[0]:
                        if "disabled" not in str(build_button.get_attribute("class")):
                            smallest_building = [percent_to_max, build_button]
            # clicks the smallest building, and returns it has expanded one
            if smallest_building[0] < 1:
                print("Smallest building: " + str(smallest_building))
                smallest_building[1].click()
                amount_castles_expanded += 1
                print("castles expanded: " + str(amount_castles_expanded))
                Lak.overlay_appeared()
                return True

        else:
            print("this castle is already fully build or has 2 expansions running")
    if amount_castles_expanded > 0:
        return True
    else:
        return False


def build_castles():
    close_all_windows()
    time.sleep(.1)
    Lak.wait_for_element("topbarImageContainer", 1).click()
    has_castles_to_expand = True
    while has_castles_to_expand:
        rerun_if_session_ended()
        try:
            has_castles_to_expand = build_castles_help()
        except StaleElementReferenceException as e:
            print("Got an Error while Expanding the building:\n" + str(e))
            pass
        print("hasCastlestoExpand: " + str(has_castles_to_expand))
    print("has no more castles to expand")


def run():
    try:
        Lak.login(yourLoginEmail, yourLoginPassword, yourWorldID)
        start_all_missions_in_time()
        build_castles()
        reset_browser()
    except LookupError as e:
        print("Something isn't loading as it should.\n" + str(e))
        time.sleep(20)
        reset_browser()
        run()
        return


def reset_browser():
    browser.get('http://lordsandknights.com')
    time.sleep(.1)
    try:
        alert = browser.switch_to_alert()
        alert.accept()
    except:
        print("no popup window appeared")
    Lak.overlay_appeared()


debug = False


def run_auto(times_to_run):
    times_already_run = 0
    while times_to_run != 0:
        times_to_run -= 1
        times_already_run += 1
        if debug:
            run()
            time.sleep(10)
        else:
            try:
                run()
                print("\n\nScript has run " + str(times_already_run) + " times")
                print("Script has fully run. Waiting for 12 mins to rerun.")
                time.sleep(6 * 60)
                print("Script has fully run. Waiting for 6 mins to rerun.")
                time.sleep(3 * 60)
                print("Script has fully run. Waiting for 3 mins to rerun.")
                time.sleep(3 * 60)

            except Exception as e:
                print(e)
                print("Script has crashed. Maybe another person logged in simultaneously")
                print("Waiting 20 secs, then trying again.")
                time.sleep(20)
                reset_browser()
                run()
            finally:
                pass


if __name__ == "__main__":
    run_auto(runXTimes)
