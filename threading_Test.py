import threading
import LakBotClass
import sqliteparser
from queue import Queue
import time


print_lock = threading.Lock()

write_lock = threading.Lock()
cond = threading.Condition(write_lock)

print("in threading: ", write_lock)


def example_job(worker):

    for i in range(200):
        with cond:
            print(threading.current_thread(), worker, cond, write_lock)





def threader():
    while True:
        worker = q.get()
        example_job(worker)
        q.task_done()


q = Queue()

# Thread count
for x in range(10):
    t = threading.Thread(target=threader)
    t.deamon = True
    t.start()

start = time.time()

#Work Count
for worker in range(10):
    q.put(worker)

# db = sqliteparser.SqliteParser(write_lock)
#
# for user in db.read_userdata_from_db():
#     q.put(user)

q.join()

print("Entire job took: ", time.time() - start)
exit()
