from builtins import print
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.common.action_chains import ActionChains
import random
import datetime
import time
import MainLibFile as Lak

browser = Lak.browser

# User Date
desiredAttackDate = datetime.datetime(2016, 3, 10, 8, 0)
final_output = ""
global_fakes = 0
runXTimes = -1
speed = False
f_name = "userdata.txt"


# General Functions
def close_all_windows():
    # finds all Xes and clicks them
    try:
        for x in browser.find_elements_by_class_name("close"):
            if x.is_displayed():
                x.click()
    except:
        print("all windows already Closed")


def calc_best_unit_for_distance(all_distances):
    perfect_distance = [1, 11.1], [2, 12.7], [101, 7.9], [102, 9.5], [201, 4.8], [202, 6.3]
    time_diff = (desiredAttackDate - datetime.datetime.now()).seconds / 60
    # Calculates the best distance of each unit
    for idx, perf_dist in enumerate(perfect_distance):
        print("Index: " + str(idx) + " Val: " + str(perf_dist))
        perfect_distance[idx][1] = time_diff / Lak.unit_speed[perf_dist[0]]
    print(perfect_distance)

    # lowest_castle=["index", "distance", "troop"]
    lowest_castle = [[0, 20000, 1], [], []]

    for idx, val in enumerate(all_distances):
        distance = [int(s) for s in val.text.split() if s.isdigit()][0]
        for idx_this, perf_dist in enumerate(perfect_distance):
            difference = abs(perf_dist[1] - distance)
            print(difference)
            if difference < lowest_castle[0][1]:
                lowest_castle[2] = lowest_castle[1]
                lowest_castle[1] = lowest_castle[0]
                lowest_castle[0] = [idx, difference, perf_dist[0]]
                print(lowest_castle)

    return lowest_castle


def fake_from_all_castles(link):
    times_attacked = 0
    Lak.wait_for_element("transports", 0).click()
    try:
        Lak.wait_for_element("sync", 0).click()
    except:
        print("Can't find any running attack to sync the fake with.")
        return
    Lak.overlay_appeared()
    own_castles_text_field = browser.find_elements_by_xpath('//*[@class="sendStuffList"]/*[@class="listItem"]')
    castle_count = 0
    total_amount = len(own_castles_text_field)
    # Loops through every individual castle
    for ownCastle in own_castles_text_field:
        Lak.accept_popup()
        time.sleep(.2)
        castle_count += 1
        # print number current castle
        print("Castle: " + str(castle_count) + " of " + str(total_amount))
        # breaks half of the castles and skips them
        if random.randint(0, 3) > 2:
            print("Skipped this castle.\n")
            continue

        # Ckecks if the castle is in Range
        points = ownCastle.find_element_by_class_name("points").text.split()[0]
        fields = ownCastle.find_element_by_class_name("fields").text.split()[0]
        if fields > points:
            continue

        # Checks if it can send swords to attack
        own_castle_swords = ownCastle.find_element_by_xpath('.//input[@data-primary-key="2"]')
        if own_castle_swords.get_attribute("disabled") != "true":
            own_castle_swords.clear()
            own_castle_swords.send_keys('1')
            time.sleep(.1)
            ownCastle.find_element_by_xpath('.//*[@class="button action syncAttack"]').click()
            # print("Faked From Castle: " + ownCastle.find_element_by_class_name("title").text.decode(
            #         'unicode_escape').encode('ascii', 'ignore'))
            times_attacked += 1
            if speed:
                Lak.overlay_appeared()
            else:
                sl = 7 + random.randint(0, 4)
                print("Waiting " + str(sl) + " seconds for the next attack\n")
                time.sleep(sl)
        else:
            # If swords are not in time, uses bows to attack
            own_castle_bows = ownCastle.find_element_by_xpath('.//input[@data-primary-key="101"]')
            if own_castle_bows.get_attribute("disabled") != "true":
                own_castle_bows.clear()
                own_castle_bows.send_keys('1')
                time.sleep(.2)
                ownCastle.find_element_by_xpath('.//*[@class="button action syncAttack"]').click()
                # print("Faked From Castle: " + ownCastle.find_element_by_class_name("title").text.decode(
                #        'unicode_escape').encode('ascii', 'ignore'))
                times_attacked += 1
                if speed:
                    Lak.overlay_appeared()
                else:
                    sl = 7 + random.randint(0, 4)
                    print("Waiting " + str(sl) + " seconds for the next attack\n")
                    time.sleep(sl)
            else:
                # If bows are not in time, uses lances to attack
                own_castle_lance = ownCastle.find_element_by_xpath('.//input[@data-primary-key="202"]')
                if own_castle_lance.get_attribute("disabled") != "true":
                    own_castle_lance.clear()
                    own_castle_lance.send_keys('1')
                    time.sleep(.2)
                    ownCastle.find_element_by_xpath('.//*[@class="button action syncAttack"]').click()
                    # print("Faked From Castle: " + ownCastle.find_element_by_class_name("title").text.decode(
                    #        'unicode_escape').encode('ascii', 'ignore'))
                    times_attacked += 1
                    if speed:
                        Lak.overlay_appeared()
                    else:
                        sl = 7 + random.randint(0, 4)
                        print("Waiting " + str(sl) + " seconds for the next attack\n")
                        time.sleep(sl)
                else:
                    # print not in range if not even lances can reach it
                    print("Castle not in range\n")
    close_all_windows()
    print("Faked castle " + str(link) + " from " + str(times_attacked) + " castles.")
    global final_output, global_fakes
    global_fakes += times_attacked
    final_output += "\nFaked castle " + str(link) + " from " + str(times_attacked) + " castles."


def open_castle(link):
    browser.execute_script("window.linkHandler('" + str(link) + "')")
    Lak.overlay_appeared()
    time.sleep(3)
    canvas = browser.find_element_by_id("map")
    castle_click_action = ActionChains(browser).move_by_offset(0, 0).click(canvas)
    castle_click_action.perform()
    Lak.overlay_appeared()


def attack_castle(link):
    close_all_windows()
    open_castle(link)
    Lak.wait_for_element("attack").click()
    Lak.overlay_appeared()
    time.sleep(.3)
    print("now ready")
    own_castles = browser.find_elements_by_xpath('//*[@class="sendStuffList"]/*[@class="listItem"]')

    distances = browser.find_elements_by_xpath(
            '//*[@class="sendStuffList"]/*[@class="listItem"]//*[@class="fields"]')
    best_castle_idx = calc_best_unit_for_distance(distances)

    current_castle = own_castles[best_castle_idx[0][0]]
    text_field = current_castle.find_element_by_xpath(
            './/input[@data-primary-key="' + str(best_castle_idx[0][2]) + '"]')
    # Checks if it can send the troops with this castle, if not, jumps to next possible castle
    if text_field.get_attribute("disabled") == "true" \
            or int(text_field.get_attribute("placeholder")) < 1:
        current_castle = own_castles[best_castle_idx[1][0]]
        text_field = current_castle.find_element_by_xpath(
                './/input[@data-primary-key="' + str(best_castle_idx[0][2]) + '"]')
    # Sets the amount of troops to send and clicks enter
    text_field.send_keys("1" + Keys.ENTER)
    time.sleep(.1)
    while True:
        time.sleep(.05)
        current_castle.find_element_by_xpath('.//*[@class="button action attack"]').click()
        try:
            browser.find_element_by_xpath(
                    '//div[contains(@class, "win foreignAction")]//div[@class="overlay"]')
            break
        except NoSuchElementException:
            pass
    time.sleep(.2)

    # Accepts attack button when attack is in the night
    try:
        browser.find_element_by_xpath("//*[contains(text(), 'OK')]").click()
    except:
        pass
    # castle_name = current_castle.find_element_by_class_name("title").text
    # castle_name = filter(lambda x: x in string.printable, castle_name)
    # print("Attacked From Castle: " + castle_name)
    sl = 10 + random.randint(0, 4)
    print("Waiting " + str(sl) + " seconds for the next attack\n")
    time.sleep(sl)

    close_all_windows()
    open_castle(link)
    Lak.overlay_appeared()
    fake_from_all_castles(link)


def fake_castle_stack_attacks_running(link_stack):
    for single_link in link_stack:
        close_all_windows()
        open_castle(single_link)
        Lak.overlay_appeared()
        fake_from_all_castles(single_link)
        Lak.overlay_appeared()
        time.sleep(3)


def fake_castle_stack(link_stack):
    for single_link in link_stack:
        attack_castle(single_link)
        Lak.overlay_appeared()
        time.sleep(3)


def file_bool_to_real(string):
    if str.lower(string) == "true":
        return True
    else:
        return False


if __name__ == "__main__" or __name__ == "builtins":
    # Asks for the User data
    print("welcome to the LAK Fake bot.")
    # yourLoginEmail = input("Type your Email Address:\n")
    # yourLoginPassword = input("Type your Password:\n")
    # yourWorldID = input("Type your world ID: (118 for D15)\n")
    # desiredAttackDate = datetime.datetime.strptime(
    #        input("When do you want the last mission to be finished\nFormat: DD.MM.YYYY MM:HH\n"), "%d.%m.%Y %H:%M")

    # Gets User data from File in same directory:
    lines = [line.rstrip('\n') for line in open(f_name)]
    print(lines)

    yourLoginEmail = lines[1]
    yourLoginPassword = lines[3]
    yourWorldID = lines[5]
    desiredAttackDate = datetime.datetime.strptime(lines[7], "%d.%m.%Y %H:%M")
    attack_running = file_bool_to_real(lines[9])
    speed = file_bool_to_real(lines[11])

    del lines[0:13]
    castles = lines

    print(yourLoginEmail + "\n" + yourLoginPassword + "\n" + yourWorldID + "\n" + str(desiredAttackDate))
    print(castles)
    print(desiredAttackDate, attack_running, speed)


    # logs in
    Lak.login(yourLoginEmail, yourLoginPassword, yourWorldID)
    Lak.wait_for_element("map", 0, "id")
    Lak.wait_for_element("topbarImageContainer", 6, "class name", 200)
    time.sleep(4)

    # decides wether the attacks are running or not
    if attack_running:
        fake_castle_stack_attacks_running(castles)
    else:
        fake_castle_stack(castles)
    final_output += "\nFake from " + str(global_fakes) + " Castles in total."
    print(final_output)


# fake_castle_stack(["l+k://coordinates?16325,16246&118", "l+k://coordinates?16329,16241&118"])

# browser.execute_script("window.linkHandler('l+k://coordinates?16301,16233&118')")


# JS function when calling link
# window.linkHandler('l+k://coordinates?16301,16233&118')

# Canvas ID: map
# canvas = browser.find_element_by_id("map")
# drawing = ActionChains(browser).move_by_offset(0, 0).click(canvas)
# drawing.perform()
