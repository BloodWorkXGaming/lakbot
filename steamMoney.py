import time
from selenium import webdriver


def count_current():
    scroll_all_way()
    prices = browser.find_elements_by_class_name("discount_final_price")
    p = 0
    for i in prices:
        s = i.text
        # s  = "1,2€"
        s = s.replace(",", ".")
        s = s.replace("€", "")
        try:
            p += float(s)
        except:
            pass

    return p


def scroll_all_way():
    y_limit = 0
    y_limt_2 = 1
    while y_limit != y_limt_2:
        y_limit = int(browser.execute_script("return scrollMaxY"))
        browser.execute_script("scroll(0," + str(y_limit) + ")")
        time.sleep(1)
        y_limt_2 = int(browser.execute_script("return scrollMaxY;"))
    return True


def get_name():
    return browser.find_element_by_class_name("curator_name").text


def cycle_all():
    all_curators = browser.find_elements_by_class_name("steam_curator_row_ctn")
    for i in range(len(all_curators)):
        all_curators[i].click()
        print(get_name() + " : " + str(round(count_current(), 2)) + "€ ")
        time.sleep(.5)
        browser.back()
        time.sleep(.5)
        all_curators = browser.find_elements_by_class_name("steam_curator_row_ctn")


browser = webdriver.Firefox()
browser.get("http://store.steampowered.com/curators/")

while False:
    inp = input("Nagivate to the Collection you wanna inspect and scroll all the way down, then press enter.")
    if inp == "exit":
        exit()

    print(get_name() + " : " + str(count_current()))

# GameStar : 3159.579999999988
# Die Gronkh Kollektion : 1877.6500000000008
# c't zockt : 1839.6900000000007
# Die Sarazar Kollektion : 1574.8200000000004
# Offizielle PC Games Community : 2825.1399999999935
# German_Gamer_Community : 2756.18999999999
# Beam - Let's Play Platz : 648.7400000000001
# for UNCUT! : 1593.760000000001
# dlhnet : 6378.059999999931
# Superlevel : 2097.69