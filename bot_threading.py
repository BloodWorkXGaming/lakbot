import threading
import LakBotClass
import sqliteparser
from queue import Queue
import time

print_lock = threading.Lock()

write_lock = threading.Lock()

print("in threading: ", write_lock)

all_bots = []


def example_job(worker):
    # with print_lock:
    own_db = sqliteparser.SqliteParser(write_lock)
    own_db.lock_account(worker[0][1], worker[0][3])
    bot = LakBotClass.LakBotClass(worker[0][1], worker[0][2], worker[0][3], write_lock)
    all_bots.append(bot)
    try:
        bot.login()
        # bot.fake_from_db()

        if worker[1] == "expand":
            bot.start_action()
        elif worker[1] == "fake":
            bot.fake_from_db()
    finally:
        own_db.unlock_account(worker[0][1], worker[0][3])
    bot.reset_browser()
    bot.browser.close()
    print(threading.current_thread(), worker)

    time.sleep(2)


def threader():
    while True:
        worker = q.get()
        example_job(worker)
        q.task_done()


q = Queue()

# Thread count
for x in range(10):
    t = threading.Thread(target=threader)
    t.daemon = True
    t.start()

start = time.time()

# Work Count
# for worker in range(2):
#    q.put(worker)

db = sqliteparser.SqliteParser(write_lock)


# starts the expansion of all the castles
def start_bot(bot_type):
    for user in db.read_unlocked_userdata_from_db():
        q.put((user, bot_type))


q.join()


print("Entire job took: ", time.time() - start)
