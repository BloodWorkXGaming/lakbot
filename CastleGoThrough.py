import MainLibFile as Lak
import time
import sqliteparser as db
from selenium.common.exceptions import NoSuchElementException
import itertools

browser = Lak.browser

max_building_level = {
    "Townhall": 10,
    "Barracks": 30,
    "Tavern Quarter": 10,
    "University": 10,
    "Fortress Wall": 30,
    "Marketplace": 10,
    "Farm": 30,
    "Sawmill": 30,
    "Wood Storage": 20,
    "Stonecutter": 30,
    "Stone Storage": 20,
    "Forge": 30,
    "Ore Storage": 20,
    "Keep": 10,
    "Arsenal": 30,
    "Tavern": 10,
    "Library": 10,
    "Fortifications": 20,
    "Market": 8,
    "Lumberjack": 30,
    "Wood store": 20,
    "Quarry": 30,
    "Stone store": 20,
    "Ore mine": 30,
    "Ore store": 20
}
is_fortress = None
base_troopspeed = {
    "speer": 11.7,
    "sword": 13.3,
    "archer": 8.3,
    "crossbow": 10.0,
    "scorpion": 5.0,
    "lance": 6.7,
    "cart": 13.3,
    "ox": 16.7
}
castle_name = None
email = None
password = None
worldid = None


# logs into every account in the db
def login_sql():
    userdata = db.read_userdata_from_db()

    for idx, row in enumerate(userdata):
        if idx == 0:
            pass
            # continue
        global email, password, worldid
        email = row[1]
        password = row[2]
        worldid = row[3]
        print(email, password, worldid)
        Lak.login(email, password, worldid)
        start_action()
        Lak.reset_browser()


# function that starts all the actions
def start_action():
    db.delete_all_castles_player(email, worldid)
    # waits for the top toolbar to appear
    Lak.wait_for_element("topbarImageContainer", 0)
    time.sleep(.5)
    Lak.kill_event_popup()
    time.sleep(.5)
    try:
        cycle_through_castle()
    except:
        cycle_through_castle()


# get Castle list
def check_fortress():
    global is_fortress
    try:
        browser.find_element_by_class_name("fortresswall")
        is_fortress = True
    except NoSuchElementException:
        is_fortress = False
    global castle_name
    castle_name = ""

    for c in itertools.takewhile(lambda c: castle_name == "", itertools.count()):
        castle_name = browser.find_elements_by_xpath("//div[@class='title smart-truncate']")[c].text
        # Lak.wait_for_element("//div[@class='title smart-truncate']", c, "xpath").text
        print(c, ": ", castle_name)


def cycle_through_castle():
    while True:
        # Start of the castle specific code
        check_fortress()
        expand_buildings()
        start_check_research()

        # opens next castle
        try:
            browser.find_element_by_xpath("//div[@class='visitCastle tab']").click()
        finally:
            try:
                browser.find_element_by_xpath("//div[@class='gfxButton headerButton paginate next']").click()
            except NoSuchElementException:
                print("all castles have been cycled through")
                return


def expand_buildings():
    try:
        browser.find_element_by_xpath("//div[@class='buildingList tab']").click()
    except NoSuchElementException:
        pass
    while True:
        currently_expanding = len(browser.find_elements_by_xpath('//div[@class="buildingUpgrade"]/div'))
        can_be_expanded = len(
            browser.find_elements_by_xpath('//*[@class="fixedBuildingList"]//*[@class="button buildbutton"]'))
        print("are: ", currently_expanding, "can: ", can_be_expanded)
        if currently_expanding < 2 and can_be_expanded > 0:
            lowest_building = (1, None)
            all_buildings = browser.find_elements_by_xpath('//div[@class="fixedBuildingList"]//div[@class="building"]')
            # find_elements_by_class_name("building")

            for building in all_buildings:
                building_name = building.find_element_by_class_name("buildingName").text
                max_level = max_building_level[building_name]
                upgrading_castles = browser.find_elements_by_xpath(
                    '//div[@class="buildingUpgrade"]/div[@class="title buildingName"]')
                current_level = int(building.find_element_by_class_name("level").text)
                for i in upgrading_castles:
                    if i.text == building_name:
                        current_level += 1
                print(building_name, " : ", current_level, " / ", max_level)
                if (current_level / max_level) < lowest_building[0]:
                    if "disabled" not in str(
                            building.find_element_by_class_name("buildbutton").get_attribute("class")):
                        lowest_building = (current_level / max_level, building)
            if lowest_building[0] < 1:
                lowest_building[1].find_element_by_class_name("buildbutton").click()
                Lak.overlay_appeared()
        else:
            break

            # class="building"


def start_check_research():
    try:
        browser.find_element_by_xpath("//div[@class='buildingList tab']").click()
        Lak.overlay_appeared()
        time.sleep(.3)
    except NoSuchElementException:
        pass
    browser.find_elements_by_xpath('//*[@class="fixedBuildingList"]//*[@class="building"]')[3].click()
    Lak.overlay_appeared()
    time.sleep(.3)
    all_research = browser.find_elements_by_xpath('//div[@class="knowledgeListItem"]')
    troopspeed = {
        "speer": 11.7,
        "sword": 13.3,
        "archer": 8.3,
        "crossbow": 10.0,
        "scorpion": 5.0,
        "lance": 6.7,
        "cart": 13.3,
        "ox": 16.7
    }
    # Checks for the speed each unit has
    for idx, single_research in enumerate(all_research):
        if is_fortress:
            # 16
            if idx == 16:
                try:
                    single_research.find_element_by_class_name("knowledgeComplete")
                    for i in troopspeed:
                        troopspeed[i] = round(troopspeed[i] * 0.9025, 1)
                except NoSuchElementException:
                    pass
            pass
        else:
            # 18
            if idx == 18:
                try:
                    single_research.find_element_by_class_name("knowledgeComplete")
                    for i in troopspeed:
                        troopspeed[i] = round(troopspeed[i] * 0.95, 1)
                except NoSuchElementException:
                    pass

    # Looks whether it can start researches and starts if true
    current_research = len(browser.find_elements_by_xpath('//div[@class="knowledgeListItem"]//*[@class="counter"]'))
    while current_research < 1:
        # not (current_research > 0 and is_fortress) or (current_research > 1 and not is_fortress):
        buttons = browser.find_elements_by_xpath('//div[@class="knowledgeListItem"]//*[@class="button"]')
        for b in buttons:
            try:
                b.find_element_by_xpath('/..//div[@class="counter"')
                b -= 1
                continue
            except NoSuchElementException:
                b.click()
                Lak.overlay_appeared()
                break
        current_research = len(browser.find_elements_by_xpath('//div[@class="knowledgeListItem"]//*[@class="counter"]'))
        if (len(browser.find_elements_by_xpath(
                '//div[@class="knowledgeListItem"]//*[@class="button"]')) - current_research) < 1:
            break

    # Pushes the speed of the units to the db
    db.add_castle_player(castle_name, 0, 0, 0, email, worldid, str(troopspeed))
    # can be converted back from string by using
    # import ast
    # ast.literal_eval(STRING)


# counter

login_sql()


# /tmp/phantomjs-2.1.1-linux-i686/bin
