import sqlite3
import threading
import ast


class SqliteParser:
    conn = None
    c = None
    cond = threading.Condition()

    def __init__(self, lock):
        self.conn = sqlite3.connect("lak.db")
        self.c = self.conn.cursor()
        self.cond = threading.Condition(lock)
        print("in sqlite parser = ", lock)
        self.create_table()

    def create_table(self):
        with self.cond:
            self.c.execute(
                'CREATE TABLE  IF NOT EXISTS "fakecastles" (`CastleName` TEXT, `link` TEXT,'
                ' `amount` INTEGER, `faked` INTEGER, `account` TEXT)')
            self.c.execute(
                'CREATE TABLE  IF NOT EXISTS "userdata" (`Name`	TEXT, `Email` TEXT, `Password` TEXT, `WorldID` TEXT,'
                ' `allowFake` INTEGER, `locked`	INTEGER)')
            self.c.execute('CREATE TABLE IF NOT EXISTS "settings" (`setting` TEXT UNIQUE, `value`	TEXT)')
            self.c.execute(
                'CREATE TABLE  IF NOT EXISTS "playercastles" (`Name` TEXT,`PosX` INTEGER,`PosY` INTEGER,'
                ' `Datahabit` INTEGER, `email` TEXT, `worldid` TEXT, `troopspeed` TEXT)')
            self.conn.commit()

        self.default_settings()

    # Userdata calls
    def read_userdata_from_db(self,):
        self.c.execute("SELECT * FROM userdata")
        all_users = []
        for row in self.c.fetchall():
            all_users.append(row)
        # print(all_users)
        return all_users

        # reads unly the unlocked users from the db
    def read_unlocked_userdata_from_db(self,):
        self.c.execute("SELECT * FROM userdata WHERE locked==0")
        all_users = []
        for row in self.c.fetchall():
            all_users.append(row)
        # print(all_users)
        return all_users

    def read_specific_user(self, email, worldid):
        self.c.execute("SELECT * FROM userdata WHERE Email == ? AND WorldID == ?", (email, worldid))
        return self.c.fetchone()

    def lock_account(self, email, worldid):
        with self.cond:
            self.c.execute("UPDATE userdata SET locked=1 WHERE email == ? AND worldid == ?", (email, worldid))
            self.conn.commit()

    def unlock_account(self, email, worldid):
        with self.cond:
            self.c.execute("UPDATE userdata SET locked=0 WHERE email == ? AND worldid == ?", (email, worldid))
            self.conn.commit()

    def unlock_all(self):
        self.c.execute("UPDATE userdata SET locked=0")
        self.conn.commit()

    # removes the entry with a given name
    def remove_user_from_db(self, email, worldid):
        with self.cond:
            self.c.execute("DELETE FROM userdata WHERE Email == ? AND WorldID == ?", (str(email), str(worldid)))
            self.conn.commit()

    # adds another user to the list
    def add_user(self, name, email, password, worldid, use_for_fakes):
        self.c.execute("SELECT * FROM userdata WHERE Email = ? AND worldid = ?", (email, worldid))
        if len(self.c.fetchall()) < 1:
            with self.cond:
                self.c.execute("INSERT INTO userdata VALUES (? , ? , ? , ? , ?, 0)",
                               (name, email, password, worldid, use_for_fakes))
                self.conn.commit()

    # Fake Castles list
    # adds a castle to the fake list
    def add_castle_fake(self, link):
        with self.cond:
            self.c.execute("INSERT INTO fakecastles VALUES (? , ? , ? , ? , ?)", ("---", link, 0, 0, "---"))
            self.conn.commit()

    # clears all fakes out of the table
    def delete_all_fakes(self):
        with self.cond:
            self.c.execute("DELETE FROM fakecastles")
            self.conn.commit()

    # deletes a single fake based on the link
    def delete_single_fake(self):
        with self.cond:
            self.c.execute("DELETE FROM fakecastle WHERE link==?")

    # gives back all the fakes
    def get_all_fakes(self):
        self.c.execute("SELECT _rowid_, * FROM fakecastles ORDER BY _rowid_ ASC")
        all_fakes = []
        for a in self.c.fetchall():
            all_fakes.append(a)
        # print(all_fakes)
        return all_fakes

    # gets one castle it has to fake
    def get_single_fake(self):

        try:
            self.c.execute("SELECT link FROM fakecastles WHERE faked == 0")
            fake_link = self.c.fetchone()[0]
        except TypeError:
            fake_link = None

        # if it gets an link it changes it to currently being attacked
        if fake_link is not None:
            with self.cond:
                self.c.execute("UPDATE fakecastles SET faked=1 WHERE link == '" + str(fake_link) + "'")
                self.conn.commit()
        return fake_link

    # sets the castle to faked and sets the amount of castles faked from and the account where it was faked from.
    def fake_finished(self, link, amount, account):
        with self.cond:
            self.c.execute("UPDATE fakecastles SET faked=2, amount= ?, account= ? WHERE link == ?",
                           (amount, account, link))
            self.conn.commit()

    # Player Castle calls
    # deletes all the castles of a single account
    def delete_all_castles_player(self, email, worldid):
        with self.cond:
            self.c.execute("DELETE FROM playercastles WHERE email == ? AND worldid == ? ", (email, worldid))
            self.conn.commit()

    # adds a single castle to the db
    def add_castle_player(self, name, posx, posy, datahabit, email, worldid, troopspeed):
        with self.cond:
            self.c.execute("INSERT INTO playercastles VALUES (? , ? , ? , ? , ?, ?, ?)",
                           (name, posx, posy, datahabit, email, worldid, troopspeed))
        self.conn.commit()

    # gets the troopspeed of a castle from a player
    def get_troopspeed(self, email, world_id, castlename):
        self.c.execute("SELECT troopspeed FROM playercastles WHERE email == ? AND worldid == ? AND Name == ?",
                       (email, str(world_id), str(castlename)))

        a = self.c.fetchall()[0][0]
        return ast.literal_eval(a)

    # Settings functions
    # creates the default settings if they are not already existent
    def default_settings(self):
        self.set_setting("attack_sleep_max", 11)
        self.set_setting("attack_sleep_min", 7)
        self.set_setting("fake_castle_amount", 30)
        self.set_setting("max_speed", 0)
        self.set_setting("attack_date", "01.01.2016 08:00")

    def set_setting(self, setting_name, value):
        with self.cond:
            self.c.execute('INSERT OR IGNORE INTO settings VALUES (?, ?)', (setting_name, value))
            self.conn.commit()

    def update_setting(self, setting_name, value):
        with self.cond:
            self.c.execute('UPDATE settings SET  value = ? WHERE setting==?', (value, setting_name))
            self.conn.commit()

    def get_setting(self, setting_name):
        self.c.execute('SELECT value FROM settings WHERE setting == ?', [str(setting_name)])
        value = self.c.fetchone()[0]
        return value

    # closes the connection
    def close_conn(self):
        self.c.close()
        self.conn.close()


if __name__ == "__main__":
    # read_userdata_from_db()
    # add_user("jj", "jj@gmail.com", "123", "118")
    # remove_user_from_db("asd", "asda")
    # read_userdata_from_db()
    add_castle_fake("link:223123,123123")
    get_all_fakes()
    # delete_all_fakes()
    close_conn()


# import sqliteparser
# import threading
# db = sqliteparser.SqliteParser(threading.Lock())#
