from pywin.debugger.fail import a
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import time
import datetime
import MainLibFile as Lak

# User Data:
attacked_castle_link = "l+k://coordinates?16305,16250&118"
attacked_castle_name = None
attacked_time = "20:58"
attacked_date = "2016/01/27"
current_troop_id = 1

browser = Lak.browser
troop_dic = {1: 0, 2: 0, 101: 0, 102: 0, 201: 0, 202: 0}
troop_dic_next = {1: 2, 2: 101, 101: 102, 102: 201, 201: 202, 202: 1}
troop_dic_next_name = {1: 1, 2: 2, 101: 3, 102: 4,
                       201: 5, 202: 0}


# {1: "Swordsman", 2: "Archer", 101: "Crossbowman", 102: "Armoured Horseman",
#                       201: "Lancer Horseman", 202: "Spearman"}

# 1: spear 2: sword 101: bow 102: crossbow 201: horseman 202: lance


def get_troop_amount(link):
    Lak.open_castle(link)
    global attacked_castle_name
    attacked_castle_name = browser.find_element_by_xpath('//*[@class="title smart-truncate"]').text
    if attacked_castle_name == '':
        attacked_castle_name = browser.find_elements_by_xpath('//*[@class="title smart-truncate"]')[1].text
    print(attacked_castle_name)
    try:
        browser.find_element_by_xpath('//*[@class="unitList tab"]').click()
    except NoSuchElementException:
        print("Tab already open")
    Lak.overlay_appeared()
    time.sleep(.3)
    browser.find_element_by_xpath('//div[@class="unitList listing contentCurrentView"]/div').click()
    elem = browser.find_element_by_xpath('//*[@class="content-frame"][1]')
    all_troops = elem.find_elements_by_class_name('unitElement')
    total_troops = 0

    # Goes trough every type of troop and sorts hands and oxes out
    for single_troop in all_troops:
        print(single_troop.get_attribute("data-primary-key"))
        if int(single_troop.get_attribute("data-primary-key")) != 10002 and int(
                single_troop.get_attribute("data-primary-key")) != 10001:
            this_troop = int(single_troop.find_element_by_class_name("affordable").text)
            this_troop -= this_troop % 51
            total_troops += this_troop
            troop_dic[int(single_troop.get_attribute("data-primary-key"))] = this_troop

    print("This castle has a total of " + str(total_troops) + " Troops.")
    print(troop_dic)
    Lak.close_all_windows()
    return total_troops


def troop_dic_next_func():
    global current_troop_id
    current_troop_id = troop_dic_next[current_troop_id]
    return troop_dic_next_name[current_troop_id]


def send_res_to_castle(send_to_link, send_from_name, troop_id):
    Lak.open_castle(send_to_link)
    time.sleep(.5)
    try:
        Lak.wait_for_element('sendResources').click()
    except LookupError:
        browser.find_elements_by_xpath('//div[@class="specials"]//div[@class="button"]')[1].click()
    Lak.overlay_appeared()
    time.sleep(2)
    print(troop_id)
    send_from_castle = browser.find_element_by_xpath("//*[contains(text(), '" + send_from_name + "')]/../../..")
    send_from_castle.find_element_by_xpath('.//input[@data-primary-key="' + str(troop_id) + '"]').send_keys(
            "51" + Keys.ENTER)
    send_from_castle.find_elements_by_xpath('.//input[@data-primary-key="1"]')[1].send_keys(
            "1" + Keys.ENTER)
    troop_dic[troop_id] -= 51

    # Clicks the button until the loading bar appears
    send_from_castle.find_element_by_class_name('sendResources').click()
    #    while True:
    #        time.sleep(.05)
    #        send_from_castle.find_element_by_class_name('sendResources').click()
    #        try:
    #
    #            browser.find_element_by_xpath(
    #                    '//div[contains(@class, "win foreignAction sendResources frame-container")]//div[@class="overlay"]')
    #            break
    #        except NoSuchElementException:
    #            pass
    time.sleep(2)
    Lak.close_all_windows()
    Lak.close_all_windows()


trickle_browser = None


def trickle_castle(link):
    global trickle_browser
    trickle_browser = webdriver.Firefox()
    trickle_browser.get("http://lordsandknights.enjoyed.today/SelfTrickle/")
    # Inserts the list of needed Data in the side
    trickle_browser.find_element_by_name("ctl00$Body$TextBoxCastleLink").send_keys(link)
    trickle_browser.find_element_by_name("ctl00$Body$TextBoxTime").send_keys(attacked_time)
    trickle_browser.find_element_by_name("ctl00$Body$TextBoxDate").send_keys(attacked_date)
    # trickle_browser.find_element_by_id("CheckBoxHarden").click()
    trickle_browser.find_element_by_id("Body_TextBoxTroopNumbers").send_keys(
            str(get_troop_amount(link)) + Keys.ENTER)

    time.sleep(4)
    # Gets the list of the castles
    condition = True
    table_parts = trickle_browser.find_elements_by_xpath('//*[@id = "TrickleTable"]//tr')
    while condition:
        for single_send_to in table_parts:

            try:
                link_text = single_send_to.find_element_by_xpath(".//a").text
                print(link_text)
                if troop_dic[current_troop_id] >= 51:
                    try:
                        send_res_to_castle(link_text, attacked_castle_name, current_troop_id)
                        single_send_to.find_element_by_xpath('.//input[@type="checkbox"]').click()
                    except Exception as e:
                        print("An Error occoured while trying to trickle this castle:\n" + str(e))
                else:
                    trickle_browser.find_element_by_xpath(
                            '//select/option[@value="' + str(troop_dic_next_func()) + '"]').click()

            except NoSuchElementException:
                print("Can't send to this castle yet")
        condition = (len(table_parts) - 2) > len(
                trickle_browser.find_elements_by_xpath('//*[@id = "TrickleTable"]//tr[@class="cover"]'))
        time.sleep(6)


Lak.login(Lak.yourLoginEmail, Lak.yourLoginPassword, Lak.yourWorldID)

test_send_from = "l+k://coordinates?16302,16246&118"
test_send_to = "l+k://coordinates?16303,16246&118"

# trickle_browser.find_elements_by_xpath('//*[@id = "TrickleTable"]//tr[@class="cover"]')
# trickle_castle("l+k://coordinates?16305,16250&118")


# l+k://coordinates?16305,16250&118


# trickle_castle("l+k://coordinates?16305,16250&118")
