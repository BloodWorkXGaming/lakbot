import datetime
import itertools
import math
import os
import random
import threading
import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

import sqliteparser

# file lock
db_lock = threading.Lock()


class LakBotClass:
    # write lock for the db
    global db_lock
    db = None
    # List of all the max levels of the buildings
    max_building_level = {
        "Townhall": 10,
        "Barracks": 30,
        "Tavern Quarter": 10,
        "University": 10,
        "Fortress Wall": 30,
        "Marketplace": 10,
        "Farm": 30,
        "Sawmill": 30,
        "Wood Storage": 20,
        "Stonecutter": 30,
        "Stone Storage": 20,
        "Forge": 30,
        "Ore Storage": 20,
        "Keep": 10,
        "Arsenal": 30,
        "Tavern": 10,
        "Library": 10,
        "Fortifications": 20,
        "Market": 8,
        "Lumberjack": 30,
        "Wood store": 20,
        "Quarry": 30,
        "Stone store": 20,
        "Ore mine": 30,
        "Ore store": 20
    }
    unit_name_to_number = {
        "speer": 1,
        "sword": 2,
        "archer": 101,
        "crossbow": 102,
        "scorpion": 201,
        "lance": 202,
        "cart": 10001,
        "ox": 10002
    }

    # Init of the Bot
    def __init__(self, email, password, world_id, write_lock, browser_window=None):
        # gives the lock to the sqlite parser
        self.db = sqliteparser.SqliteParser(write_lock)
        print("in lakbotclass: ", write_lock)

        version_number = "1_1"
        self.email = email
        self.password = password
        self.world_id = world_id

        # init the given class vars
        self.is_fortress = None
        self.castle_name = None
        # settings of the db
        self.speed = bool(int(self.db.get_setting("max_speed")))
        self.wait_time_base = int(self.db.get_setting("attack_sleep_min"))
        self.wait_time_sleep = int(self.db.get_setting("attack_sleep_max")) - int(
            self.db.get_setting("attack_sleep_min"))
        self.fake_castle_amount = int(self.db.get_setting("fake_castle_amount"))
        self.attack_date = datetime.datetime.strptime(self.db.get_setting("attack_date"), "%d.%m.%Y %H:%M")

        print(self.attack_date)
        # self.attack_date = datetime.datetime(2016, 3, 11, 8, 0)


        print("Name of the object : ", id(self), id(self.is_fortress), id(self.email))

        # As Debug, you can setup a file that has the firefox path in it
        line = None
        if os.path.isfile("firefox_dir.txt"):
            with open("firefox_dir.txt") as f:
                line = f.readline().replace("\\", "/")
                print(str(line))
        self.browser = webdriver.Firefox(executable_path=line)

        # if browser_window is not None:
        #    self.browser = browser_window
        #    # implements webdriver

        # browser = webdriver.PhantomJS()
        self.browser.set_window_size(1400, 800)
        self.browser.get('http://lordsandknights.com/')

        # General Functions

    def wait_for_element(self, element_name, index=0, by='class name', timeout_time=80):
        # waits until the missions button is clickable
        ready = False
        times_run = 0
        while not ready:
            time.sleep(.2)
            try:
                waiting_for = self.browser.find_elements(by, element_name)[index]
                print("\nPage is ready!")
                ready = True
                return waiting_for
            except NoSuchElementException:
                if times_run == 0:
                    print("( " + element_name + ") Element not found!", end="", flush=True)
                else:
                    if times_run > 0:
                        print("!", end="", flush=True)
                times_run += 1
            except IndexError:
                if times_run == 0:
                    print("( " + element_name + ") Element not found!", end="", flush=True)
                else:
                    if times_run > 0:
                        print("!", end="", flush=True)
            times_run += 1
            if times_run > timeout_time:
                raise LookupError("Page not loading. waiting for element: '" + str(element_name) + "'")

    # logs the user into the game
    def login(self):
        self.wait_for_element("popup-link").click()
        time.sleep(.4)

        # inserts email address
        elem = self.wait_for_element("loginEmail", 0, "name")
        elem.clear()
        elem.send_keys(self.email + Keys.TAB)
        time.sleep(.5)
        # inserts password and presses enter
        self.wait_for_element("loginPassword", 0, "name").send_keys(self.password + Keys.RETURN)

        # waits until the worldbutton is clickable
        self.wait_for_element(self.world_id, 0, "id").click()
        print("Logged in, load page")

    # pauses the thread until it has loaded
    def overlay_appeared(self):
        # waits until the overlay disappears
        times_run = 0
        while True:
            time.sleep(.2)
            try:
                self.browser.find_element_by_class_name("overlay")
                if times_run == 0:
                    print("Overlay still there!", end="", flush=True)
                else:
                    if times_run > 0:
                        print("!", end="", flush=True)
                times_run += 1
            except NoSuchElementException:
                print("\nOverlay disappeared!")
                return
        time.sleep(1)

    # closes all the active windows
    def close_all_windows(self):
        try:
            # while len(all_x) > 0:
            all_x = self.browser.find_elements_by_class_name("close")
            for x in all_x:
                print("Closing all Windows")
                if x.is_displayed():
                    x.click()
        # all_x = browser.find_elements_by_class_name("close")
        except:
            print("all windows already Closed")

        time.sleep(.2)
        # finds all Xes and clicks them
        # all_x = [1]

    # opens a castle using a link
    def open_castle(self, link):
        self.close_all_windows()
        self.browser.execute_script("window.linkHandler('" + str(link) + "')")
        self.overlay_appeared()
        time.sleep(2)
        canvas = self.browser.find_element_by_id("map")
        castle_click_action = ActionChains(self.browser).move_by_offset(0, 0).click(canvas)
        castle_click_action.perform()
        self.overlay_appeared()
        time.sleep(1)

    # accepts a popup if it appears
    def accept_popup(self):
        time.sleep(.3)
        try:
            self.browser.find_elements_by_xpath('//div[contains(@class, "win dialog")]//*[@class="button"]').click()
            time.sleep(.5)
            return True
        except:
            return False

    # resets the browser to the main page
    def reset_browser(self):
        self.browser.get('http://lordsandknights.com')
        time.sleep(.1)
        try:
            alert = self.browser.switch_to_alert()
            alert.accept()
        except:
            print("no popup window appeared")
        self.overlay_appeared()

    # clicks the event button if a new event started
    def kill_event_popup(self):
        try:
            self.browser.find_element_by_class_name("event-button").click()
        except NoSuchElementException:
            pass

    # function that starts all the actions
    def start_action(self):
        self.db.delete_all_castles_player(self.email, self.world_id)
        # waits for the top toolbar to appear
        self.wait_for_element("topbarImageContainer", 0)
        time.sleep(.5)
        self.kill_event_popup()
        time.sleep(.5)
        try:
            self.cycle_through_castle()
        except:
            self.cycle_through_castle()

    # checks if the caslte is a fortress or not
    def check_is_fortress(self):
        try:
            self.browser.find_element_by_class_name("fortresswall")
            self.is_fortress = True
        except NoSuchElementException:
            self.is_fortress = False

    # gets the name of the current castle
    def get_castle_name(self):
        self.castle_name = ""
        for c in itertools.takewhile(lambda c: self.castle_name == "", itertools.count()):
            try:
                self.castle_name = self.wait_for_element("//div[@class='title smart-truncate']", c, "xpath").text
            except:
                pass
            # Lak.wait_for_element("//div[@class='title smart-truncate']", c, "xpath").text
            return self.castle_name

    # goes through every castle and performs actions
    def cycle_through_castle(self):
        while True:
            # Start of the castle specific code
            self.check_is_fortress()
            self.get_castle_name()
            # self.expand_buildings()
            self.start_check_research()

            # opens next castle
            try:
                try:
                    time.sleep(.5)
                    self.browser.find_element_by_xpath("//div[@class='visitCastle tab']").click()
                except NoSuchElementException:
                    try:
                        self.kill_event_popup()
                        time.sleep(1)
                    except NoSuchElementException:
                        pass
            finally:
                try:
                    time.sleep(.5)
                    self.browser.find_element_by_xpath("//div[@class='gfxButton headerButton paginate next']").click()
                except NoSuchElementException:
                    print("all castles have been cycled through")
                    return

    # expands the buildings in the current castle
    def expand_buildings(self):
        try:
            self.browser.find_element_by_xpath("//div[@class='buildingList tab']").click()
        except NoSuchElementException:
            pass
        while True:
            currently_expanding = len(self.browser.find_elements_by_xpath('//div[@class="buildingUpgrade"]/div'))
            can_be_expanded = len(
                self.browser.find_elements_by_xpath('//*[@class="fixedBuildingList"]//*[@class="button buildbutton"]'))
            print("are: ", currently_expanding, "can: ", can_be_expanded)
            if currently_expanding < 2 and can_be_expanded > 0:
                try:
                    self.browser.find_element_by_xpath('//div[@class="buildingList tab active"]').click()
                except:
                    pass
                lowest_building = (1, None)
                all_buildings = self.browser.find_elements_by_xpath(
                    '//div[@class="fixedBuildingList"]//div[@class="building"]')
                # find_elements_by_class_name("building")

                for building in all_buildings:
                    # building_name = building.find_element_by_class_name("buildingName").text
                    building_name = building.find_element_by_xpath('.//div[@class="title buildingName"]').text
                    max_level = self.max_building_level[building_name]
                    upgrading_castles = self.browser.find_elements_by_xpath(
                        '//div[@class="buildingUpgrade"]/div[@class="title buildingName"]')
                    current_level = int(building.find_element_by_class_name("level").text)
                    for i in upgrading_castles:
                        if i.text == building_name:
                            current_level += 1
                    print(building_name, " : ", current_level, " / ", max_level)
                    if (current_level / max_level) < lowest_building[0]:
                        if "disabled" not in str(
                                building.find_element_by_class_name("buildbutton").get_attribute("class")):
                            lowest_building = (current_level / max_level, building)
                if lowest_building[0] < 1:
                    lowest_building[1].find_element_by_class_name("buildbutton").click()
                    self.overlay_appeared()
            else:
                break

                # class="building"

    # starts the researches and calculates the individual unit speeds
    def start_check_research(self):
        try:
            self.browser.find_element_by_xpath("//div[@class='buildingList tab']").click()
            self.overlay_appeared()
            time.sleep(.3)
        except NoSuchElementException:
            pass
        self.browser.find_elements_by_xpath('//*[@class="fixedBuildingList"]//*[@class="building"]')[3].click()
        self.overlay_appeared()
        time.sleep(.3)
        all_research = self.browser.find_elements_by_xpath('//div[@class="knowledgeListItem"]')
        troop_speed = {
            "speer": 11.7,
            "sword": 13.3,
            "archer": 8.3,
            "crossbow": 10.0,
            "scorpion": 5.0,
            "lance": 6.7,
            "cart": 13.3,
            "ox": 16.7
        }
        # Checks for the speed each unit has
        for idx, single_research in enumerate(all_research):
            if self.is_fortress:
                # 16
                if idx == 16:
                    try:
                        single_research.find_element_by_class_name("knowledgeComplete")
                        for i in troop_speed:
                            troop_speed[i] = round(troop_speed[i] * 0.9025, 1)
                    except NoSuchElementException:
                        pass
                pass
            else:
                # 18
                if idx == 18:
                    try:
                        single_research.find_element_by_class_name("knowledgeComplete")
                        for i in troop_speed:
                            troop_speed[i] = round(troop_speed[i] * 0.95, 1)
                    except NoSuchElementException:
                        pass

        # Looks whether it can start researches and starts if true --> can be disabled here.
        if False:
            current_research = len(
                self.browser.find_elements_by_xpath('//div[@class="knowledgeListItem"]//*[@class="counter"]'))
            while current_research < 1:
                # not (current_research > 0 and is_fortress) or (current_research > 1 and not is_fortress):
                buttons = self.browser.find_elements_by_xpath('//div[@class="knowledgeListItem"]//*[@class="button"]')
                for b in buttons:
                    try:
                        b.find_element_by_xpath('/..//div[@class="counter"')
                        b -= 1
                        continue
                    except NoSuchElementException:
                        b.click()
                        self.overlay_appeared()
                        break
                current_research = len(
                    self.browser.find_elements_by_xpath('//div[@class="knowledgeListItem"]//*[@class="counter"]'))
                if (len(self.browser.find_elements_by_xpath(
                        '//div[@class="knowledgeListItem"]//*[@class="button"]')) - current_research) < 1:
                    break
            try:
                self.browser.find_element_by_xpath("//div[@class='visitCastle tab']").click()
            except:
                pass

        # Pushes the speed of the units to the db
        self.db.add_castle_player(self.castle_name, 0, 0, 0, self.email, self.world_id, str(troop_speed))
        # can be converted back from string by using
        # import ast
        # ast.literal_eval(STRING)

    # All functions from auto fake castle

    # fakes the given link from all the casltes
    def fake_from_all_castles(self, link):
        times_attacked = 0
        # opens the sync window and checks if a attack is already running
        self.wait_for_element("transports", 0).click()
        time.sleep(.3)
        try:
            self.wait_for_element("transitrow")
            all_syncs = self.browser.find_elements_by_class_name("transitrow")
            closest_row = [[None, math.inf]]
            for sync in all_syncs:
                try:
                    date = datetime.datetime.strptime(sync.find_element_by_class_name("caption").text, "%d.%m.%Y %H:%M")
                except ValueError:
                    continue
                time_diff = (self.attack_date - date).days * 24 * 60 + (self.attack_date - date).seconds / 60
                if time_diff < closest_row[0][1]:
                    closest_row.insert(0, [sync, time_diff])
            closest_row[0][0].find_element_by_class_name("sync").click()
        except (NoSuchElementException, LookupError):
            print("Can't find any running attack to sync the fake with.")
            return
        self.overlay_appeared()
        own_castles_text_field = self.browser.find_elements_by_xpath('//*[@class="sendStuffList"]/*[@class="listItem"]')
        castle_count = 0
        total_amount = len(own_castles_text_field)

        # makes a list with the length of all castles but only uses 30:
        castle_amount_list = [False] * total_amount
        base = self.fake_castle_amount + random.randint(-2, 2)
        if len(castle_amount_list) < base:
            base = len(castle_amount_list)

        while base > 0:
            n = random.randint(0, len(castle_amount_list) - 1)
            if not castle_amount_list[n]:
                base -= 1
                castle_amount_list[n] = True

        # Loops through every individual castle
        for idx, ownCastle in enumerate(own_castles_text_field):
            self.accept_popup()
            time.sleep(.2)
            castle_count += 1
            # print number current castle
            print("Castle: " + str(castle_count) + " of " + str(total_amount))

            # breaks if the castle is not in the list
            if not castle_amount_list[idx]:
                print("Skipped the castle")
                continue

            # Ckecks if the castle is in Range
            points = ownCastle.find_element_by_class_name("points").text.split()[0]
            fields = ownCastle.find_element_by_class_name("fields").text.split()[0]
            if fields > points:
                continue

            # Checks if it can send swords to attack
            own_castle_swords = ownCastle.find_element_by_xpath('.//input[@data-primary-key="2"]')
            # tries to attack with swords
            if self.send_attack(2, 1, ownCastle):
                times_attacked += 1
            else:
                # tries with bows afterwards
                if self.send_attack(101, 1, ownCastle):
                    times_attacked += 1
                else:
                    # tries with lances last
                    if self.send_attack(202, 1, ownCastle):
                        times_attacked += 1
                    else:
                        print("Castle not in range to fake it.\n")

        self.close_all_windows()
        print("Faked castle " + str(link) + " from " + str(times_attacked) + " castles.")
        self.db.fake_finished(link, times_attacked, self.email)

    # starts a attack with a given unit from a given caslte
    def send_attack(self, unit_type, amount, castle_list_entry):
        unit_textbox = castle_list_entry.find_element_by_xpath('.//input[@data-primary-key="' + str(unit_type) + '"]')
        if unit_textbox.get_attribute("disabled") != "true" and int(unit_textbox.get_attribute("placeholder")) > 0:
            unit_textbox.clear()
            unit_textbox.send_keys(amount)
            time.sleep(.1)
            # castle_list_entry.find_element_by_xpath('.//*[@class="button action syncAttack"]').click()
            # tries to lick the button more often until the overlay finally appears
            while True:
                castle_list_entry.find_element_by_xpath('.//*[contains(@class, "button action")]').click()
                try:
                    self.browser.find_element_by_class_name("overlay")
                    break
                except NoSuchElementException:
                    continue

            if self.speed:
                self.overlay_appeared()
            else:
                sl = self.wait_time_base + random.randint(0, self.wait_time_sleep)
                print("Waiting " + str(sl) + " seconds for the next attack\n")
                time.sleep(sl)
            return True
        return False

    # attacks the castle with the most accurate timing
    def attack_castle(self, link, attackdate):
        # opens the attack window of the castle it wants to attack
        self.close_all_windows()
        self.open_castle(link)
        self.wait_for_element("attack").click()
        self.overlay_appeared()
        time.sleep(.3)
        print("now ready")

        # gets the castles with the best distance
        best_castle = self.calc_best_castle(attackdate)
        castle_index = 0
        while True:
            attack_castle = best_castle[castle_index]
            if self.send_attack(int(self.unit_name_to_number[attack_castle[2]]), 1, attack_castle[3]):
                break
            else:
                castle_index += 1

        # Accepts attack button when attack is in the night
        try:
            self.browser.find_element_by_xpath("//*[contains(text(), 'OK')]").click()
        except NoSuchElementException:
            pass

        self.close_all_windows()
        self.open_castle(link)
        self.overlay_appeared()
        self.fake_from_all_castles(link)

    # calcs the best castle to attack from to be able to sync
    def calc_best_castle(self, desired_attack_date):
        time_diff_datetime = (desired_attack_date - datetime.datetime.now())
        time_diff = time_diff_datetime.days * 24 * 60 + time_diff_datetime.seconds / 60
        all_castles = self.browser.find_elements_by_xpath('//div[@class="sendStuffList"]//div[@class="listItem"]')
        lowest_castle = [["Name", math.inf, "type", None]]
        for single_castle in all_castles:
            castle_name_current = single_castle.find_element_by_xpath('.//div[@class= "title smart-truncate"]').text
            troopspeed = self.db.get_troopspeed(self.email, self.world_id, castle_name_current)
            distance = int(single_castle.find_element_by_class_name("fields").text.split()[0])

            # breaks the current caslte if it can't reach it due to too less points
            if distance > int(single_castle.find_element_by_class_name("points").text.split()[0]):
                continue

            for unit in troopspeed:
                time_needed = troopspeed[unit] * int(distance)
                time_diff_to_attack = math.fabs(time_diff - time_needed)
                if lowest_castle[0][1] > time_diff_to_attack:
                    lowest_castle.insert(0, [castle_name_current, time_diff_to_attack, unit, single_castle])
        return lowest_castle

    # goes through every link it has to fake the castles of the db
    def fake_from_db(self):
        self.wait_for_element("topbarImageContainer", 0)
        time.sleep(.5)
        self.kill_event_popup()
        time.sleep(.5)
        # goes through every castle and starts faking them
        while True:
            if self.db.read_specific_user(self.email, self.world_id)[4] == 0:
                return
            fake_link = self.db.get_single_fake()
            if fake_link is None:
                return
            else:
                self.attack_castle(fake_link, self.attack_date)

    # def attack until all the fake castles in the db are already faked to
    def fake_castle_stack_attacks_running(self, link_stack):
        for single_link in link_stack:
            self.close_all_windows()
            self.open_castle(single_link)
            self.overlay_appeared()
            self.fake_from_all_castles(single_link)
            self.overlay_appeared()
            time.sleep(3)

    def fake_castle_stack(self, link_stack):
        for single_link in link_stack:
            self.attack_castle(single_link)
            self.overlay_appeared()
            time.sleep(3)


if __name__ == "__main__":
    bot = LakBotClass("bloodworkxgaming@gmail.com", "shadow123", "118", threading.Lock())
    bot.login()
    bot.start_action()
